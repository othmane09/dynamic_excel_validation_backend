CREATE TABLE excel.excel_column
(
    id                    character varying(36) NOT NULL,
    description           text,
    name                  character varying(50),
    "position"            integer,
    required              boolean,
    column_foreign_key_id character varying(36),
    column_unique_key_id  character varying(36),
    excel_table_id        character varying(36) NOT NULL
);
