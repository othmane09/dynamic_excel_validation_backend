CREATE TABLE excel.column_foreign_key
(
    id                     character varying(36) NOT NULL,
    schema                 character varying(50),
    table_id_column        character varying(50),
    table_name             character varying(50),
    table_reference_column character varying(50),
    excel_column_id        character varying(36)
);