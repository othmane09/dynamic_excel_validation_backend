INSERT INTO excel.column_validation (id, description, name, pattern, type)
VALUES ('0ed7c738-6005-4507-aa43-bf205800f3ab', 'THE VALUE MAST BE NUMBER', 'NUMBER', '-?\d+(\.\d+)?', 'REGEX_PATTERN');
INSERT INTO excel.column_validation (id, description, name, pattern, type)
VALUES ('785d7087-9298-46bd-a0bc-821e2d16a9af', 'THE VALUE MAST BE EMAIL', 'EMAIL',
        '\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b', 'REGEX_PATTERN');
INSERT INTO excel.column_validation (id, description, name, pattern, type)
VALUES ('46d166be-7d6a-4375-bbb8-dacfa646a907', 'THE VALUE MAST BE DATE', 'DATE', 'd/M/uuuu', 'REGEX_DATE');
INSERT INTO excel.column_validation (id, description, name, pattern, type)
VALUES ('7d949762-4716-47bc-a982-81383373b261', 'THIS WILL ACCEPT EVERYTHING', 'NONE', '', 'NONE');