CREATE TABLE excel.column_validation
(
    id          character varying(36) NOT NULL,
    description text,
    name        character varying(50),
    pattern     character varying(255),
    type        character varying(255)
);