CREATE TABLE excel.table_validation
(
    id          character varying(36) NOT NULL,
    description text,
    name        character varying(50)
);
