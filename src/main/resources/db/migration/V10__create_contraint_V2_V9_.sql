ALTER TABLE ONLY excel.column_foreign_key
    ADD CONSTRAINT column_foreign_key_pkey PRIMARY KEY (id);


ALTER TABLE ONLY excel.column_unique_key
    ADD CONSTRAINT column_unique_key_pkey PRIMARY KEY (id);



ALTER TABLE ONLY excel.column_validation
    ADD CONSTRAINT column_validation_pkey PRIMARY KEY (id);



ALTER TABLE ONLY excel.excel_column
    ADD CONSTRAINT excel_column_pkey PRIMARY KEY (id);


ALTER TABLE ONLY excel.excel_table
    ADD CONSTRAINT excel_table_pkey PRIMARY KEY (id);



ALTER TABLE ONLY excel.table_validation
    ADD CONSTRAINT table_validation_pkey PRIMARY KEY (id);



ALTER TABLE ONLY excel.excel_table
    ADD CONSTRAINT uk_l8l2i1tehdsjwqbvuvgytjs2 UNIQUE (name);



ALTER TABLE ONLY excel.column_unique_key
    ADD CONSTRAINT fk21ak37n6xln70gpxk4u435u3n FOREIGN KEY (excel_column_id) REFERENCES excel.excel_column(id);


ALTER TABLE ONLY excel.excel_tables_table_validations
    ADD CONSTRAINT fk34ltmkyqtgp6lx4q0pbv6cg41 FOREIGN KEY (excel_table_id) REFERENCES excel.excel_table(id);



ALTER TABLE ONLY excel.excel_columns_column_validations
    ADD CONSTRAINT fkb7d5ge6qw53fvj4o0j931ovt1 FOREIGN KEY (excel_column_id) REFERENCES excel.excel_column(id);



ALTER TABLE ONLY excel.excel_column
    ADD CONSTRAINT fkefk834vgse5rv0qvwbyd6rkcb FOREIGN KEY (excel_table_id) REFERENCES excel.excel_table(id);



ALTER TABLE ONLY excel.column_foreign_key
    ADD CONSTRAINT fkfpoq73v5bsy2sagw68ckpc29d FOREIGN KEY (excel_column_id) REFERENCES excel.excel_column(id);


ALTER TABLE ONLY excel.excel_column
    ADD CONSTRAINT fki9oy1i2khfxuy2f8xu01w0xyp FOREIGN KEY (column_unique_key_id) REFERENCES excel.column_unique_key(id);


ALTER TABLE ONLY excel.excel_columns_column_validations
    ADD CONSTRAINT fkp8bc0scp689j0mfk717vpwknl FOREIGN KEY (column_validation_id) REFERENCES excel.column_validation(id);


ALTER TABLE ONLY excel.excel_tables_table_validations
    ADD CONSTRAINT fksrsof1wpso4jwc4j0ti39vfcw FOREIGN KEY (table_validation_id) REFERENCES excel.table_validation(id);


ALTER TABLE ONLY excel.excel_column
    ADD CONSTRAINT fktlr0b0jujvdg2gtd0sk9689rd FOREIGN KEY (column_foreign_key_id) REFERENCES excel.column_foreign_key(id);

