CREATE TABLE excel.excel_table
(
    id                     character varying(36) NOT NULL,
    description            text,
    external_server_insert boolean DEFAULT false,
    name                   character varying(50),
    url                    character varying(250)
);
