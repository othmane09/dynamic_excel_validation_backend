package dz.blida.exercise.relationnel.repostirories;

import dz.blida.exercise.relationnel.models.ColumnUniqueKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ColumnUniqueKeyRepository extends JpaRepository<ColumnUniqueKey, String> {
    @Query(
            value = "select cuk.* from excel.excel_table et\n" +
                    "inner join excel.excel_column ec on et.id = ec.excel_table_id\n" +
                    "inner join excel.column_unique_key cuk on cuk.id = ec.column_unique_key_id\n" +
                    "where et.name = ?1",
            nativeQuery = true)
    List<ColumnUniqueKey> findByExcelTableName(String tableName);
}
