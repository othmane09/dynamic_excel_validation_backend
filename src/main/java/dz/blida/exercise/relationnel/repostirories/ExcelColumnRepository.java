package dz.blida.exercise.relationnel.repostirories;

import dz.blida.exercise.relationnel.models.ExcelColumn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ExcelColumnRepository extends JpaRepository<ExcelColumn, String> {

    Optional<List<ExcelColumn>> findByExcelTable_IdOrderByPosition(String id);
}
