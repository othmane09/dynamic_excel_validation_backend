package dz.blida.exercise.relationnel.repostirories;

import dz.blida.exercise.relationnel.models.ExcelTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ExcelTableRepository extends JpaRepository<ExcelTable, String> {

    @Query(nativeQuery = true,
            value = "SELECT id FROM excel.excel_table WHERE name = ?1")
    Optional<String> findIdByName(String name);

    Boolean existsByName(String name);

    Optional<ExcelTable> findByName(String tableName);
}
