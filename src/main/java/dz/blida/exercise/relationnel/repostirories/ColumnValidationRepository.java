package dz.blida.exercise.relationnel.repostirories;

import dz.blida.exercise.relationnel.models.ColumnValidation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ColumnValidationRepository extends JpaRepository<ColumnValidation, String> {

    List<ColumnValidation> findByNameIn(List<String> names);

    ColumnValidation findByName(String name);

    Boolean existsByName(String name);
}
