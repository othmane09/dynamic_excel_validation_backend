package dz.blida.exercise.relationnel.repostirories;

import dz.blida.exercise.relationnel.models.TableValidation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@EnableJpaRepositories
public interface TableValidationRepository extends JpaRepository<TableValidation, String> {
    Boolean existsByName(String name);


    List<TableValidation> findByNameIn(List<String> names);

    List<TableValidation> findByIdIn(List<String> ids);
}
