package dz.blida.exercise.relationnel.repostirories;

import dz.blida.exercise.relationnel.models.ColumnForeignKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ColumnForeignKeyRepository extends JpaRepository<ColumnForeignKey, String> {
    @Query(
            value = "select cfk.* from excel.excel_table et\n" +
                    "inner join excel.excel_column ec on et.id = ec.excel_table_id\n" +
                    "inner join excel.column_foreign_key cfk on cfk.id = ec.column_foreign_key_id\n" +
                    "where et.name = ?1 ",
            nativeQuery = true)
    List<ColumnForeignKey> findByExcelTableName(String tableName);
}
