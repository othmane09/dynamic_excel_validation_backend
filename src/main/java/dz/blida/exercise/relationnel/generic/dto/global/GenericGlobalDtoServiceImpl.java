package dz.blida.exercise.relationnel.generic.dto.global;

import dz.blida.exercise.relationnel.generic.dto.basic.GenericBasicDtoServiceImpl;
import dz.blida.exercise.shared.exeptions.ResponseException;
import dz.blida.exercise.shared.generic.dto.global.GenericGlobalDtoMapper;
import dz.blida.exercise.shared.generic.dto.global.GenericGlobalDtoService;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public class GenericGlobalDtoServiceImpl<ENTITY, SIMPLE_DTO, GLOBAL_DTO, REPO extends JpaRepository, MAP extends GenericGlobalDtoMapper, ID>
        extends GenericBasicDtoServiceImpl<ENTITY, SIMPLE_DTO, REPO, MAP, ID>
        implements GenericGlobalDtoService<ENTITY, SIMPLE_DTO, GLOBAL_DTO, ID> {

    MAP entityMapper;

    public GenericGlobalDtoServiceImpl(REPO entityRepository, MAP entityMapper) {
        super(entityRepository, entityMapper);
        this.entityMapper = entityMapper;
    }

    @Override
    public GLOBAL_DTO findByIdAsGlobalDto(ID id) {
        if (id == null) return null;
        Optional<ENTITY> entityRecord = getEntityRepository().findById(id);
        if (entityRecord.isPresent())
            return (GLOBAL_DTO) this.entityMapper.entityToGlobalDto(entityRecord.get());
        return null;
    }

    @Override
    public GLOBAL_DTO findByIdAsGlobalDtoElseThrowException(ID id) {
        if (id == null)
            throw new ResponseException("Id is Null Please try to enter some value next time");
        Optional<ENTITY> entityRecord = getEntityRepository().findById(id);
        if (entityRecord.isPresent())
            return (GLOBAL_DTO) this.entityMapper.entityToGlobalDto(entityRecord.get());
        throw new ResponseException("No Recode Match With This Id ??");
    }

    @Override
    public List<GLOBAL_DTO> findAllAsGlobalDtos() {
        return this.entityMapper.entitiesToGlobalDtos(getEntityRepository().findAll());
    }

    @Override
    public List<GLOBAL_DTO> findAllAsGlobalDtosElseThrowException() {
        List<ENTITY> entityRecords = getEntityRepository().findAll();
        if (entityRecords == null)
            throw new ResponseException("The Is No Record Of This Entity");
        return this.entityMapper.entitiesToGlobalDtos(entityRecords);
    }

    @Override
    public GLOBAL_DTO saveFromGlobalDto(GLOBAL_DTO entity) {
        return (GLOBAL_DTO) this.entityMapper.entityToGlobalDto(getEntityRepository().save(this.entityMapper.globalDtoToEntity(entity)));
    }

    @Override
    public List<GLOBAL_DTO> saveAllFromGlobalDtos(Iterable<GLOBAL_DTO> entities) {
        return (List<GLOBAL_DTO>) this.entityMapper.globalDtoToEntity(getEntityRepository().saveAll(this.entityMapper.globalDtosToEntities((List) entities)));
    }

    @Override
    public void deleteFromGlobalDto(GLOBAL_DTO entity) {
        try {
            getEntityRepository().delete(this.entityMapper.globalDtoToEntity(entity));
        } catch (Exception e) {
            throw new ResponseException("This Records Cant Be Deleted Contact Admin For More Information");
        }
    }

    @Override
    public void deleteAllFromGlobalDto(Iterable<GLOBAL_DTO> entities) {
        try {
            getEntityRepository().deleteAll(this.entityMapper.globalDtosToEntities((List) entities));
        } catch (Exception e) {
            throw new ResponseException("This Records Cant Be Deleted Contact Admin For More Information");
        }
    }

    @Override
    public MAP getEntityMapper() {
        return this.entityMapper;
    }
}
