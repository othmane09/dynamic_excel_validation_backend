package dz.blida.exercise.relationnel.controllers;


import dz.blida.exercise.relationnel.models.ExcelColumn;
import dz.blida.exercise.relationnel.services.excelColumn.dto.basic.ExcelColumnBasicDto;
import dz.blida.exercise.relationnel.services.excelColumn.dto.basic.ExcelColumnBasicDtoService;
import dz.blida.exercise.shared.generic.dto.basic.GenericBasicDtoController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/excel_columns")
public class ExcelColumnControllerBasic extends GenericBasicDtoController<ExcelColumn, ExcelColumnBasicDto, ExcelColumnBasicDtoService, String> {

    public ExcelColumnControllerBasic(ExcelColumnBasicDtoService entityService) {
        super(entityService);
    }
}
