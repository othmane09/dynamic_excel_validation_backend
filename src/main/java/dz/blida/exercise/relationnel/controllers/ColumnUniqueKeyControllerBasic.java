package dz.blida.exercise.relationnel.controllers;


import dz.blida.exercise.relationnel.models.ColumnUniqueKey;
import dz.blida.exercise.relationnel.services.columnUniqueKey.dto.basic.ColumnUniqueKeyBasicDto;
import dz.blida.exercise.relationnel.services.columnUniqueKey.dto.basic.ColumnUniqueKeyBasicDtoService;
import dz.blida.exercise.shared.generic.dto.basic.GenericBasicDtoController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/column_unique_keys")
public class ColumnUniqueKeyControllerBasic extends GenericBasicDtoController<ColumnUniqueKey, ColumnUniqueKeyBasicDto, ColumnUniqueKeyBasicDtoService, String> {

    public ColumnUniqueKeyControllerBasic(ColumnUniqueKeyBasicDtoService entityService) {
        super(entityService);
    }
}
