package dz.blida.exercise.relationnel.controllers;


import dz.blida.exercise.relationnel.models.ExcelTable;
import dz.blida.exercise.relationnel.services.excelTable.dto.basic.ExcelTableBasicDto;
import dz.blida.exercise.relationnel.services.excelTable.dto.global.ExcelTableGlobalDto;
import dz.blida.exercise.relationnel.services.excelTable.dto.global.ExcelTableGlobalDtoService;
import dz.blida.exercise.shared.generic.dto.global.GenericGlobalDtoController;
import dz.blida.exercise.shared.response.Response;
import dz.blida.exercise.shared.response.ResponseService;
import dz.blida.exercise.shared.services.excel.ExcelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/excel_tables")
public class ExcelTableControllerBasic extends GenericGlobalDtoController<ExcelTable, ExcelTableGlobalDto, ExcelTableBasicDto, ExcelTableGlobalDtoService, String> {
    @Autowired
    ExcelService excelService;

    public ExcelTableControllerBasic(ExcelTableGlobalDtoService entityService) {
        super(entityService);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Response> delete(@PathVariable("id") String id) {
        excelService.deleteExcelTable(id);
        return ResponseService.success("Deleted successfully!");
    }
}
