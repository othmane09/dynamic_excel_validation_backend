package dz.blida.exercise.relationnel.controllers;


import dz.blida.exercise.relationnel.models.ColumnValidation;
import dz.blida.exercise.relationnel.services.columnValidation.dto.basic.ColumnValidationBasicDto;
import dz.blida.exercise.relationnel.services.columnValidation.dto.basic.ColumnValidationBasicDtoService;
import dz.blida.exercise.shared.generic.dto.basic.GenericBasicDtoController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/column_validations")
public class ColumnValidationControllerBasic extends GenericBasicDtoController<ColumnValidation, ColumnValidationBasicDto, ColumnValidationBasicDtoService, String> {

    public ColumnValidationControllerBasic(ColumnValidationBasicDtoService entityService) {
        super(entityService);
    }
}
