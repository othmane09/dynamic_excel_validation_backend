package dz.blida.exercise.relationnel.controllers;


import dz.blida.exercise.relationnel.models.TableValidation;
import dz.blida.exercise.relationnel.services.tableValidation.dto.models.basic.TableValidationBasicDto;
import dz.blida.exercise.relationnel.services.tableValidation.dto.models.basic.TableValidationBasicDtoService;
import dz.blida.exercise.shared.generic.dto.basic.GenericBasicDtoController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/table_validations")
public class TableValidationControllerBasic extends GenericBasicDtoController<TableValidation, TableValidationBasicDto, TableValidationBasicDtoService, String> {

    public TableValidationControllerBasic(TableValidationBasicDtoService entityService) {
        super(entityService);
    }
}
