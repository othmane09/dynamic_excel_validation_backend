package dz.blida.exercise.relationnel.controllers;


import dz.blida.exercise.relationnel.models.ColumnForeignKey;
import dz.blida.exercise.relationnel.services.columnForeignKey.dto.basic.ColumnForeignKeyBasicDto;
import dz.blida.exercise.relationnel.services.columnForeignKey.dto.basic.ColumnForeignKeyBasicDtoService;
import dz.blida.exercise.shared.generic.dto.basic.GenericBasicDtoController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/column_foreign_keys")
public class ColumnForeignKeyControllerBasic extends GenericBasicDtoController<ColumnForeignKey, ColumnForeignKeyBasicDto, ColumnForeignKeyBasicDtoService, String> {
    public ColumnForeignKeyControllerBasic(ColumnForeignKeyBasicDtoService entityService) {
        super(entityService);
    }
}
