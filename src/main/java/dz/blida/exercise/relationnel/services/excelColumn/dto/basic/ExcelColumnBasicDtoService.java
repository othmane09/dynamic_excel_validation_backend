package dz.blida.exercise.relationnel.services.excelColumn.dto.basic;


import dz.blida.exercise.relationnel.models.ExcelColumn;
import dz.blida.exercise.shared.generic.dto.basic.GenericBasicDtoService;

public interface ExcelColumnBasicDtoService extends GenericBasicDtoService<ExcelColumn, ExcelColumnBasicDto, String> {


}