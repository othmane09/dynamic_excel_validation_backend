package dz.blida.exercise.relationnel.services.columnUniqueKey.dto.basic;

import dz.blida.exercise.relationnel.generic.dto.basic.GenericBasicDtoServiceImpl;
import dz.blida.exercise.relationnel.models.ColumnUniqueKey;
import dz.blida.exercise.relationnel.repostirories.ColumnUniqueKeyRepository;
import org.springframework.stereotype.Service;


@Service
public class ColumnUniqueKeyBasicDtoServiceImpl extends GenericBasicDtoServiceImpl<ColumnUniqueKey, ColumnUniqueKeyBasicDto, ColumnUniqueKeyRepository, ColumnUniqueKeyBasicMapper, String>
        implements ColumnUniqueKeyBasicDtoService {

    public ColumnUniqueKeyBasicDtoServiceImpl(ColumnUniqueKeyRepository entityRepository, ColumnUniqueKeyBasicMapper entityMapper) {
        super(entityRepository, entityMapper);
    }
}
