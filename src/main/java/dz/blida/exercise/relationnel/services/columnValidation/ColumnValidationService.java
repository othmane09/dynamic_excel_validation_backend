package dz.blida.exercise.relationnel.services.columnValidation;


import dz.blida.exercise.relationnel.models.ColumnValidation;
import dz.blida.exercise.shared.generic.basic.GenericService;

public interface ColumnValidationService extends GenericService<ColumnValidation, String> {

}
