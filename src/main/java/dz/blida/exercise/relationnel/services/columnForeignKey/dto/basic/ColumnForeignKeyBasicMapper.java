package dz.blida.exercise.relationnel.services.columnForeignKey.dto.basic;

import dz.blida.exercise.relationnel.models.ColumnForeignKey;
import dz.blida.exercise.shared.generic.dto.basic.GenericBasicDtoMapper;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface ColumnForeignKeyBasicMapper extends GenericBasicDtoMapper<ColumnForeignKey, ColumnForeignKeyBasicDto> {

}
