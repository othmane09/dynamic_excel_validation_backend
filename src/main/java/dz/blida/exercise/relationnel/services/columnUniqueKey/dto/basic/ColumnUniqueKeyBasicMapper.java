package dz.blida.exercise.relationnel.services.columnUniqueKey.dto.basic;

import dz.blida.exercise.relationnel.models.ColumnUniqueKey;
import dz.blida.exercise.shared.generic.dto.basic.GenericBasicDtoMapper;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface ColumnUniqueKeyBasicMapper extends GenericBasicDtoMapper<ColumnUniqueKey, ColumnUniqueKeyBasicDto> {

}
