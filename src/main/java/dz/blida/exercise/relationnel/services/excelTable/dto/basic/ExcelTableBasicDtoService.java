package dz.blida.exercise.relationnel.services.excelTable.dto.basic;


import dz.blida.exercise.relationnel.models.ExcelTable;
import dz.blida.exercise.shared.generic.dto.basic.GenericBasicDtoService;

public interface ExcelTableBasicDtoService extends GenericBasicDtoService<ExcelTable, ExcelTableBasicDto, String> {


}