package dz.blida.exercise.relationnel.services.tableValidation.dto.models.global;

import lombok.Data;

import java.util.List;

@Data
public class ExcelColumnDto {

    private String id;

    private String name;

    private String description;

    private Boolean required;

    private Integer position;

    private List<ColumnValidationDto> columnValidations;

    private ColumnForeignKeyDto columnForeignKey;

    private ColumnUniqueKeyDto columnUniqueKeyDto;
}
