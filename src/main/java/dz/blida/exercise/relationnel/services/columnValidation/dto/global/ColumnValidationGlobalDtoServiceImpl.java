package dz.blida.exercise.relationnel.services.columnValidation.dto.global;


import dz.blida.exercise.relationnel.generic.dto.global.GenericGlobalDtoServiceImpl;
import dz.blida.exercise.relationnel.models.ColumnValidation;
import dz.blida.exercise.relationnel.repostirories.ColumnValidationRepository;
import dz.blida.exercise.relationnel.services.columnValidation.dto.basic.ColumnValidationBasicDto;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ColumnValidationGlobalDtoServiceImpl extends GenericGlobalDtoServiceImpl<ColumnValidation, ColumnValidationBasicDto, ColumnValidationGlobalDto, ColumnValidationRepository, ColumnValidationGlobalMapper, String>
        implements ColumnValidationGlobalDtoService {

    public ColumnValidationGlobalDtoServiceImpl(ColumnValidationRepository entityRepository, ColumnValidationGlobalMapper entityMapper) {
        super(entityRepository, entityMapper);
    }

    @Override
    public List<ColumnValidation> findByNameIn(List<String> names) {
        return getEntityRepository().findByNameIn(names);
    }

    @Override
    public ColumnValidation findByName(String name) {
        return getEntityRepository().findByName(name);
    }

    @Override
    public Boolean existsByName(String name) {
        return getEntityRepository().existsByName(name);
    }

}
