package dz.blida.exercise.relationnel.services.columnForeignKey;

import dz.blida.exercise.relationnel.generic.GenericServiceImpl;
import dz.blida.exercise.relationnel.models.ColumnForeignKey;
import dz.blida.exercise.relationnel.repostirories.ColumnForeignKeyRepository;
import org.springframework.stereotype.Service;


@Service
public class ColumnForeignKeyServiceImpl extends GenericServiceImpl<ColumnForeignKey, ColumnForeignKeyRepository, String> implements ColumnForeignKeyService {

    public ColumnForeignKeyServiceImpl(ColumnForeignKeyRepository entityRepository) {
        super(entityRepository);
    }

}
