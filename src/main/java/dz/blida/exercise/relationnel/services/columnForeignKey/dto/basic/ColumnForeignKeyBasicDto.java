package dz.blida.exercise.relationnel.services.columnForeignKey.dto.basic;

import lombok.Data;

@Data
public class ColumnForeignKeyBasicDto {

    private String id;

    private String tableName;

    private String tableReferenceColumn;

    private String tableIdColumn;

    private String schema;

}
