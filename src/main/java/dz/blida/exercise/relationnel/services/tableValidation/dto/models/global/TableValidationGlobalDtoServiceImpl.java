package dz.blida.exercise.relationnel.services.tableValidation.dto.models.global;

import dz.blida.exercise.relationnel.generic.dto.global.GenericGlobalDtoServiceImpl;
import dz.blida.exercise.relationnel.models.TableValidation;
import dz.blida.exercise.relationnel.repostirories.TableValidationRepository;
import dz.blida.exercise.relationnel.services.tableValidation.dto.models.basic.TableValidationBasicDto;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class TableValidationGlobalDtoServiceImpl extends GenericGlobalDtoServiceImpl<TableValidation, TableValidationBasicDto, TableValidationGlobalDto, TableValidationRepository, TableValidationGlobalMapper, String>
        implements TableValidationGlobalDtoService {

    public TableValidationGlobalDtoServiceImpl(TableValidationRepository entityRepository, TableValidationGlobalMapper entityMapper) {
        super(entityRepository, entityMapper);
    }

    @Override
    public Boolean existsByName(String name) {
        return getEntityRepository().existsByName(name);
    }

    @Override
    public List<TableValidation> findByNameIn(List<String> names) {
        return getEntityRepository().findByNameIn(names);
    }

    @Override
    public List<TableValidation> findByIdIn(List<String> ids) {
        return getEntityRepository().findByIdIn(ids);
    }


}
