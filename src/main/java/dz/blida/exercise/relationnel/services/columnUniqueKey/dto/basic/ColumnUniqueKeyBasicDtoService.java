package dz.blida.exercise.relationnel.services.columnUniqueKey.dto.basic;


import dz.blida.exercise.relationnel.models.ColumnUniqueKey;
import dz.blida.exercise.shared.generic.dto.basic.GenericBasicDtoService;

public interface ColumnUniqueKeyBasicDtoService extends GenericBasicDtoService<ColumnUniqueKey, ColumnUniqueKeyBasicDto, String> {

}