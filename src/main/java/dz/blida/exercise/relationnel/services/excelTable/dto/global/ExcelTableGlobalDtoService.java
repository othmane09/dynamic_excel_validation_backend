package dz.blida.exercise.relationnel.services.excelTable.dto.global;


import dz.blida.exercise.relationnel.models.ExcelTable;
import dz.blida.exercise.relationnel.services.excelTable.dto.basic.ExcelTableBasicDto;
import dz.blida.exercise.shared.generic.dto.global.GenericGlobalDtoService;

public interface ExcelTableGlobalDtoService extends GenericGlobalDtoService<ExcelTable, ExcelTableBasicDto, ExcelTableGlobalDto, String> {

    String findIdByName(String name);

    String findIdByNameElseThrowException(String name);

    Boolean existsByName(String name);

    ExcelTableGlobalDto findAllByNameElseThrowException(String tableName);

}