package dz.blida.exercise.relationnel.services.tableValidation;


import dz.blida.exercise.relationnel.models.TableValidation;
import dz.blida.exercise.shared.generic.basic.GenericService;

public interface TableValidationService extends GenericService<TableValidation, String> {

}
