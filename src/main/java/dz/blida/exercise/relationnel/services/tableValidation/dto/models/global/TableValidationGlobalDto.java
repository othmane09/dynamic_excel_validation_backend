package dz.blida.exercise.relationnel.services.tableValidation.dto.models.global;

import lombok.Data;

import java.util.List;

@Data
public class TableValidationGlobalDto {

    private String id;

    private String name;

    private String description;

    private List<ExcelTableDto> excelTables;

}
