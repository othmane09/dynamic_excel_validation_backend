package dz.blida.exercise.relationnel.services.columnUniqueKey.dto.global;


import dz.blida.exercise.relationnel.generic.dto.global.GenericGlobalDtoServiceImpl;
import dz.blida.exercise.relationnel.models.ColumnUniqueKey;
import dz.blida.exercise.relationnel.repostirories.ColumnUniqueKeyRepository;
import dz.blida.exercise.relationnel.services.columnUniqueKey.dto.basic.ColumnUniqueKeyBasicDto;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ColumnUniqueKeyGlobalDtoServiceImpl extends GenericGlobalDtoServiceImpl<ColumnUniqueKey, ColumnUniqueKeyBasicDto, ColumnUniqueKeyGlobalDto, ColumnUniqueKeyRepository, ColumnUniqueKeyGlobalMapper, String>
        implements ColumnUniqueKeyGlobalDtoService {

    public ColumnUniqueKeyGlobalDtoServiceImpl(ColumnUniqueKeyRepository entityRepository, ColumnUniqueKeyGlobalMapper entityMapper) {
        super(entityRepository, entityMapper);
    }

    @Override
    public List<ColumnUniqueKeyGlobalDto> findByExcelTableName(String tableName) {
        return getEntityMapper().entitiesToGlobalDtos(getEntityRepository().findByExcelTableName(tableName));
    }
}
