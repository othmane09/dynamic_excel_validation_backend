package dz.blida.exercise.relationnel.services.columnValidation.dto.basic;

import dz.blida.exercise.relationnel.models.ColumnValidation;
import dz.blida.exercise.shared.generic.dto.basic.GenericBasicDtoMapper;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface ColumnValidationBasicMapper extends GenericBasicDtoMapper<ColumnValidation, ColumnValidationBasicDto> {

}
