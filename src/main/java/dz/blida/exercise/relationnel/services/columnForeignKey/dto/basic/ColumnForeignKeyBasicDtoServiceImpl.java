package dz.blida.exercise.relationnel.services.columnForeignKey.dto.basic;


import dz.blida.exercise.relationnel.generic.dto.basic.GenericBasicDtoServiceImpl;
import dz.blida.exercise.relationnel.models.ColumnForeignKey;
import dz.blida.exercise.relationnel.repostirories.ColumnForeignKeyRepository;
import org.springframework.stereotype.Service;

@Service
public class ColumnForeignKeyBasicDtoServiceImpl extends GenericBasicDtoServiceImpl<ColumnForeignKey, ColumnForeignKeyBasicDto, ColumnForeignKeyRepository, ColumnForeignKeyBasicMapper, String>
        implements ColumnForeignKeyBasicDtoService {

    public ColumnForeignKeyBasicDtoServiceImpl(ColumnForeignKeyRepository entityRepository, ColumnForeignKeyBasicMapper entityMapper) {
        super(entityRepository, entityMapper);
    }


}
