package dz.blida.exercise.relationnel.services.columnUniqueKey.dto.basic;

import lombok.Data;

@Data
public class ColumnUniqueKeyBasicDto {

    private String id;

    private String tableName;

    private String tableReferenceColumn;

    private String schema;

}
