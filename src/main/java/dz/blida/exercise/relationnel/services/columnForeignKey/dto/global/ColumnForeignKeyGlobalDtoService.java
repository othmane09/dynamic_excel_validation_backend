package dz.blida.exercise.relationnel.services.columnForeignKey.dto.global;

import dz.blida.exercise.relationnel.models.ColumnForeignKey;
import dz.blida.exercise.relationnel.services.columnForeignKey.dto.basic.ColumnForeignKeyBasicDto;
import dz.blida.exercise.shared.generic.dto.global.GenericGlobalDtoService;

import java.util.List;

public interface ColumnForeignKeyGlobalDtoService extends GenericGlobalDtoService<ColumnForeignKey, ColumnForeignKeyBasicDto, ColumnForeignKeyGlobalDto, String> {

    List<ColumnForeignKeyGlobalDto> findByExcelTableName(String tableName);
}