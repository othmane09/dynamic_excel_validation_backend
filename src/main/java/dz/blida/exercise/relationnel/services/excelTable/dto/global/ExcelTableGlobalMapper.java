package dz.blida.exercise.relationnel.services.excelTable.dto.global;

import dz.blida.exercise.relationnel.models.ExcelTable;
import dz.blida.exercise.relationnel.services.excelTable.dto.basic.ExcelTableBasicDto;
import dz.blida.exercise.shared.generic.dto.global.GenericGlobalDtoMapper;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface ExcelTableGlobalMapper extends GenericGlobalDtoMapper<ExcelTable, ExcelTableBasicDto, ExcelTableGlobalDto> {

}
