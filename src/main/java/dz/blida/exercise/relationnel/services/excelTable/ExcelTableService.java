package dz.blida.exercise.relationnel.services.excelTable;


import dz.blida.exercise.relationnel.models.ExcelTable;
import dz.blida.exercise.shared.generic.basic.GenericService;

public interface ExcelTableService extends GenericService<ExcelTable, String> {

}
