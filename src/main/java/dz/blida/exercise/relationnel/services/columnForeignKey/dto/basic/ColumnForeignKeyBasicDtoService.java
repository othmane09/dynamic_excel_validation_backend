package dz.blida.exercise.relationnel.services.columnForeignKey.dto.basic;

import dz.blida.exercise.relationnel.models.ColumnForeignKey;
import dz.blida.exercise.shared.generic.dto.basic.GenericBasicDtoService;


public interface ColumnForeignKeyBasicDtoService extends GenericBasicDtoService<ColumnForeignKey, ColumnForeignKeyBasicDto, String> {

}