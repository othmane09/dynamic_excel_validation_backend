package dz.blida.exercise.relationnel.services.columnForeignKey.dto.global;

import lombok.Data;

import java.util.List;

@Data
public class ExcelColumnDto {

    private String id;

    private String name;

    private String description;

    private Boolean required;

    private Integer position;

    private ExcelTableDto excelTable;

    private List<ColumnValidationDto> columnValidations;

    private ColumnUniqueKeyDto columnUniqueKey;
}
