package dz.blida.exercise.relationnel.services.excelTable.dto.global;


import dz.blida.exercise.relationnel.generic.dto.global.GenericGlobalDtoServiceImpl;
import dz.blida.exercise.relationnel.models.ExcelTable;
import dz.blida.exercise.relationnel.repostirories.ExcelTableRepository;
import dz.blida.exercise.relationnel.services.excelTable.dto.basic.ExcelTableBasicDto;
import dz.blida.exercise.shared.exeptions.ResponseException;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class ExcelTableGlobalDtoServiceImpl extends GenericGlobalDtoServiceImpl<ExcelTable, ExcelTableBasicDto, ExcelTableGlobalDto, ExcelTableRepository, ExcelTableGlobalMapper, String>
        implements ExcelTableGlobalDtoService {

    public ExcelTableGlobalDtoServiceImpl(ExcelTableRepository entityRepository, ExcelTableGlobalMapper entityMapper) {
        super(entityRepository, entityMapper);
    }

    @Override
    public String findIdByName(String name) {
        if (name == null) return null;
        Optional<String> id = getEntityRepository().findIdByName(name);
        if (id.isPresent())
            return id.get();
        return null;
    }

    @Override
    public String findIdByNameElseThrowException(String name) {
        if (name == null) throw new ResponseException("id is null  ");
        Optional<String> id = getEntityRepository().findIdByName(name);
        if (id.isPresent())
            return id.get();
        throw new ResponseException("no record found");
    }

    @Override
    public Boolean existsByName(String name) {
        return getEntityRepository().existsByName(name);
    }

    @Override
    public ExcelTableGlobalDto findAllByNameElseThrowException(String tableName) {
        if (tableName == null) throw new ResponseException("tableName is null  ");
        Optional<ExcelTable> excelTable = getEntityRepository().findByName(tableName);
        if (excelTable.isPresent())
            return getEntityMapper().entityToGlobalDto(excelTable.get());
        throw new ResponseException("no record found");
    }

}
