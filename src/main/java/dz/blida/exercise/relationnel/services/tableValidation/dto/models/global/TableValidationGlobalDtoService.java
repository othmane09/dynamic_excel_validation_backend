package dz.blida.exercise.relationnel.services.tableValidation.dto.models.global;


import dz.blida.exercise.relationnel.models.TableValidation;
import dz.blida.exercise.relationnel.services.tableValidation.dto.models.basic.TableValidationBasicDto;
import dz.blida.exercise.shared.generic.dto.global.GenericGlobalDtoService;

import java.util.List;

public interface TableValidationGlobalDtoService extends GenericGlobalDtoService<TableValidation, TableValidationBasicDto, TableValidationGlobalDto, String> {

    Boolean existsByName(String name);

    List<TableValidation> findByNameIn(List<String> names);

    List<TableValidation> findByIdIn(List<String> ids);
}