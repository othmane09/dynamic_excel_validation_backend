package dz.blida.exercise.relationnel.services.excelTable.dto.global;

import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class ExcelTableGlobalDto {

    private String id;

    private String name;

    private String url;

    private boolean externalServerInsert = false;

    private String description;

    private List<TableValidationDto> tableValidations;

    private Set<ExcelColumnDto> excelColumns;
}
