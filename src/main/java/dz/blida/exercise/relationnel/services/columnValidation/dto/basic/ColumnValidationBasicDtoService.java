package dz.blida.exercise.relationnel.services.columnValidation.dto.basic;


import dz.blida.exercise.relationnel.models.ColumnValidation;
import dz.blida.exercise.shared.generic.dto.basic.GenericBasicDtoService;

public interface ColumnValidationBasicDtoService extends GenericBasicDtoService<ColumnValidation, ColumnValidationBasicDto, String> {

}