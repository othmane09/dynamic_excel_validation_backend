package dz.blida.exercise.relationnel.services.columnValidation.dto.global;

import dz.blida.exercise.relationnel.models.ColumnValidationType;
import lombok.Data;

import java.util.List;

@Data
public class ColumnValidationGlobalDto {

    private String id;

    private String name;

    private ColumnValidationType type;

    private String pattern;

    private String description;

    private List<ExcelColumnDto> excelColumns;
}
