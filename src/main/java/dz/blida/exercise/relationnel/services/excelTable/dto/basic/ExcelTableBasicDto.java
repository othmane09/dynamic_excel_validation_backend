package dz.blida.exercise.relationnel.services.excelTable.dto.basic;

import lombok.Data;

@Data
public class ExcelTableBasicDto {

    private String id;

    private String name;

    private boolean externalServerInsert = false;

    private String description;

}
