package dz.blida.exercise.relationnel.services.tableValidation;

import dz.blida.exercise.relationnel.generic.GenericServiceImpl;
import dz.blida.exercise.relationnel.models.TableValidation;
import dz.blida.exercise.relationnel.repostirories.TableValidationRepository;
import org.springframework.stereotype.Service;


@Service
public class TableValidationServiceImpl extends GenericServiceImpl<TableValidation, TableValidationRepository, String> implements TableValidationService {

    public TableValidationServiceImpl(TableValidationRepository entityRepository) {
        super(entityRepository);
    }

}
