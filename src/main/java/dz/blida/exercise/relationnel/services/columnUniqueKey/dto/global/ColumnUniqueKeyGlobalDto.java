package dz.blida.exercise.relationnel.services.columnUniqueKey.dto.global;

import lombok.Data;

@Data
public class ColumnUniqueKeyGlobalDto {

    private String id;

    private String tableName;

    private String tableReferenceColumn;

    private String schema;

    private ExcelColumnDto excelColumn;
}
