package dz.blida.exercise.relationnel.services.tableValidation.dto.models.global;

import lombok.Data;

@Data
public class ColumnUniqueKeyDto {

    private String id;

    private String tableName;

    private String tableReferenceColumn;

    private String schema;
}
