package dz.blida.exercise.relationnel.services.tableValidation.dto.models.global;

import dz.blida.exercise.relationnel.models.TableValidation;
import dz.blida.exercise.relationnel.services.tableValidation.dto.models.basic.TableValidationBasicDto;
import dz.blida.exercise.shared.generic.dto.global.GenericGlobalDtoMapper;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface TableValidationGlobalMapper extends GenericGlobalDtoMapper<TableValidation, TableValidationBasicDto, TableValidationGlobalDto> {

}
