package dz.blida.exercise.relationnel.services.excelTable;

import dz.blida.exercise.relationnel.generic.GenericServiceImpl;
import dz.blida.exercise.relationnel.models.ExcelTable;
import dz.blida.exercise.relationnel.repostirories.ExcelTableRepository;
import org.springframework.stereotype.Service;


@Service
public class ExcelTableServiceImpl extends GenericServiceImpl<ExcelTable, ExcelTableRepository, String> implements ExcelTableService {

    public ExcelTableServiceImpl(ExcelTableRepository entityRepository) {
        super(entityRepository);
    }

}
