package dz.blida.exercise.relationnel.services.excelColumn.dto.global;

import lombok.Data;

import java.util.List;

@Data
public class ExcelTableDto {

    private String id;

    private String name;

    private String url;

    private String description;

    private List<TableValidationDto> tableValidations;

}
