package dz.blida.exercise.relationnel.services.tableValidation.dto.models.global;

import lombok.Data;

import java.util.Set;

@Data
public class ExcelTableDto {

    private String id;

    private String name;

    private String description;

    private Set<ExcelColumnDto> excelColumns;
}
