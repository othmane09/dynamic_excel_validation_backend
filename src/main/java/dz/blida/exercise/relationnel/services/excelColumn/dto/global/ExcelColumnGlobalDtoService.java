package dz.blida.exercise.relationnel.services.excelColumn.dto.global;


import dz.blida.exercise.relationnel.models.ExcelColumn;
import dz.blida.exercise.relationnel.services.excelColumn.dto.basic.ExcelColumnBasicDto;
import dz.blida.exercise.shared.generic.dto.global.GenericGlobalDtoService;

import java.util.List;

public interface ExcelColumnGlobalDtoService extends GenericGlobalDtoService<ExcelColumn, ExcelColumnBasicDto, ExcelColumnGlobalDto, String> {

    List<ExcelColumnBasicDto> findByExcelTable_IdAsBasicElseThrowException(String id);

    List<ExcelColumnGlobalDto> findByExcelTable_IdAsGlobalElseThrowException(String id);

}