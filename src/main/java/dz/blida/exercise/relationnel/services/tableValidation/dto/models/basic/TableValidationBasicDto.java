package dz.blida.exercise.relationnel.services.tableValidation.dto.models.basic;

import lombok.Data;

@Data
public class TableValidationBasicDto {

    private String id;

    private String name;

    private String url;

    private String description;

}
