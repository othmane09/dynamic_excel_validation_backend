package dz.blida.exercise.relationnel.services.excelColumn;


import dz.blida.exercise.relationnel.models.ExcelColumn;
import dz.blida.exercise.shared.generic.basic.GenericService;

public interface ExcelColumnService extends GenericService<ExcelColumn, String> {

}
