package dz.blida.exercise.relationnel.services.columnValidation.dto.global;

import lombok.Data;

@Data
public class ExcelColumnDto {

    private String id;

    private String name;

    private String description;

    private Boolean required;

    private Integer position;

    private ExcelTableDto excelTable;

    private ColumnForeignKeyDto columnForeignKey;

    private ColumnUniqueKeyDto columnUniqueKeyDto;
}
