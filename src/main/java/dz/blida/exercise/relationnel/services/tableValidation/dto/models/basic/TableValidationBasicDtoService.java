package dz.blida.exercise.relationnel.services.tableValidation.dto.models.basic;


import dz.blida.exercise.relationnel.models.TableValidation;
import dz.blida.exercise.shared.generic.dto.basic.GenericBasicDtoService;

public interface TableValidationBasicDtoService extends GenericBasicDtoService<TableValidation, TableValidationBasicDto, String> {


}