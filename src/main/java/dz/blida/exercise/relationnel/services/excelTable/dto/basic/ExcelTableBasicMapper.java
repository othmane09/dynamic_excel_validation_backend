package dz.blida.exercise.relationnel.services.excelTable.dto.basic;

import dz.blida.exercise.relationnel.models.ExcelTable;
import dz.blida.exercise.shared.generic.dto.basic.GenericBasicDtoMapper;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface ExcelTableBasicMapper extends GenericBasicDtoMapper<ExcelTable, ExcelTableBasicDto> {

}
