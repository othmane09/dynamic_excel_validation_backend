package dz.blida.exercise.relationnel.services.tableValidation.dto.models.basic;

import dz.blida.exercise.relationnel.models.TableValidation;
import dz.blida.exercise.shared.generic.dto.basic.GenericBasicDtoMapper;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface TableValidationBasicMapper extends GenericBasicDtoMapper<TableValidation, TableValidationBasicDto> {

}
