package dz.blida.exercise.relationnel.services.excelTable.dto.basic;


import dz.blida.exercise.relationnel.generic.dto.basic.GenericBasicDtoServiceImpl;
import dz.blida.exercise.relationnel.models.ExcelTable;
import dz.blida.exercise.relationnel.repostirories.ExcelTableRepository;
import org.springframework.stereotype.Service;


@Service
public class ExcelTableBasicDtoServiceImpl extends GenericBasicDtoServiceImpl<ExcelTable, ExcelTableBasicDto, ExcelTableRepository, ExcelTableBasicMapper, String>
        implements ExcelTableBasicDtoService {

    public ExcelTableBasicDtoServiceImpl(ExcelTableRepository entityRepository, ExcelTableBasicMapper entityMapper) {
        super(entityRepository, entityMapper);
    }


}
