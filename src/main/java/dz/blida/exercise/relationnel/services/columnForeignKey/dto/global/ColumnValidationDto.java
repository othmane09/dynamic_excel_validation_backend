package dz.blida.exercise.relationnel.services.columnForeignKey.dto.global;

import lombok.Data;

@Data
public class ColumnValidationDto {

    private String id;

    private String name;

    private String description;

}
