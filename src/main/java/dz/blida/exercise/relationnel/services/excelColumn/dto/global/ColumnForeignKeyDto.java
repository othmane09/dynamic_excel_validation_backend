package dz.blida.exercise.relationnel.services.excelColumn.dto.global;

import lombok.Data;

@Data
public class ColumnForeignKeyDto {

    private String id;

    private String tableName;

    private String tableReferenceColumn;

    private String tableIdColumn;

    private String schema;

}