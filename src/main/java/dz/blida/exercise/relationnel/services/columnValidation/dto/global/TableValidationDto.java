package dz.blida.exercise.relationnel.services.columnValidation.dto.global;

import lombok.Data;

@Data
public class TableValidationDto {

    private String id;

    private String name;

    private String description;

}
