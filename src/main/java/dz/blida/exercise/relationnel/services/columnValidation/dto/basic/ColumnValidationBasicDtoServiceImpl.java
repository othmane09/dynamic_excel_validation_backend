package dz.blida.exercise.relationnel.services.columnValidation.dto.basic;


import dz.blida.exercise.relationnel.generic.dto.basic.GenericBasicDtoServiceImpl;
import dz.blida.exercise.relationnel.models.ColumnValidation;
import dz.blida.exercise.relationnel.repostirories.ColumnValidationRepository;
import org.springframework.stereotype.Service;


@Service
public class ColumnValidationBasicDtoServiceImpl extends GenericBasicDtoServiceImpl<ColumnValidation, ColumnValidationBasicDto, ColumnValidationRepository, ColumnValidationBasicMapper, String>
        implements ColumnValidationBasicDtoService {

    public ColumnValidationBasicDtoServiceImpl(ColumnValidationRepository entityRepository, ColumnValidationBasicMapper entityMapper) {
        super(entityRepository, entityMapper);
    }

}
