package dz.blida.exercise.relationnel.services.columnForeignKey;


import dz.blida.exercise.relationnel.models.ColumnForeignKey;
import dz.blida.exercise.shared.generic.basic.GenericService;

public interface ColumnForeignKeyService extends GenericService<ColumnForeignKey, String> {

}
