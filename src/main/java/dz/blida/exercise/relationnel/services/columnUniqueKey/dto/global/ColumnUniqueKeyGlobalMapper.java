package dz.blida.exercise.relationnel.services.columnUniqueKey.dto.global;

import dz.blida.exercise.relationnel.models.ColumnUniqueKey;
import dz.blida.exercise.relationnel.services.columnUniqueKey.dto.basic.ColumnUniqueKeyBasicDto;
import dz.blida.exercise.shared.generic.dto.global.GenericGlobalDtoMapper;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface ColumnUniqueKeyGlobalMapper extends GenericGlobalDtoMapper<ColumnUniqueKey, ColumnUniqueKeyBasicDto, ColumnUniqueKeyGlobalDto> {

}
