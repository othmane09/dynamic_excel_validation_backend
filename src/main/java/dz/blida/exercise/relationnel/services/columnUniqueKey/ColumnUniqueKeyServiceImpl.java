package dz.blida.exercise.relationnel.services.columnUniqueKey;

import dz.blida.exercise.relationnel.generic.GenericServiceImpl;
import dz.blida.exercise.relationnel.models.ColumnUniqueKey;
import dz.blida.exercise.relationnel.repostirories.ColumnUniqueKeyRepository;
import org.springframework.stereotype.Service;


@Service
public class ColumnUniqueKeyServiceImpl extends GenericServiceImpl<ColumnUniqueKey, ColumnUniqueKeyRepository, String> implements ColumnUniqueKeyService {

    public ColumnUniqueKeyServiceImpl(ColumnUniqueKeyRepository entityRepository) {
        super(entityRepository);
    }

}
