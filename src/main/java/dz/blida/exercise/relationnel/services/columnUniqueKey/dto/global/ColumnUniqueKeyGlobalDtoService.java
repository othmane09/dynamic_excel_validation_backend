package dz.blida.exercise.relationnel.services.columnUniqueKey.dto.global;


import dz.blida.exercise.relationnel.models.ColumnUniqueKey;
import dz.blida.exercise.relationnel.services.columnUniqueKey.dto.basic.ColumnUniqueKeyBasicDto;
import dz.blida.exercise.shared.generic.dto.global.GenericGlobalDtoService;

import java.util.List;

public interface ColumnUniqueKeyGlobalDtoService extends GenericGlobalDtoService<ColumnUniqueKey, ColumnUniqueKeyBasicDto, ColumnUniqueKeyGlobalDto, String> {

    List<ColumnUniqueKeyGlobalDto> findByExcelTableName(String tableName);

}