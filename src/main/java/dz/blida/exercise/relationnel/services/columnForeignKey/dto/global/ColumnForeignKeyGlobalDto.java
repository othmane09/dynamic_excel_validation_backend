package dz.blida.exercise.relationnel.services.columnForeignKey.dto.global;

import lombok.Data;

@Data
public class ColumnForeignKeyGlobalDto {

    private String id;

    private String tableName;

    private String tableReferenceColumn;

    private String tableIdColumn;

    private String schema;

    private ExcelColumnDto excelColumn;


}
