package dz.blida.exercise.relationnel.services.excelColumn.dto.basic;


import dz.blida.exercise.relationnel.generic.dto.basic.GenericBasicDtoServiceImpl;
import dz.blida.exercise.relationnel.models.ExcelColumn;
import dz.blida.exercise.relationnel.repostirories.ExcelColumnRepository;
import org.springframework.stereotype.Service;

@Service
public class ExcelColumnBasicDtoServiceImpl extends GenericBasicDtoServiceImpl<ExcelColumn, ExcelColumnBasicDto, ExcelColumnRepository, ExcelColumnBasicMapper, String>
        implements ExcelColumnBasicDtoService {

    public ExcelColumnBasicDtoServiceImpl(ExcelColumnRepository entityRepository, ExcelColumnBasicMapper entityMapper) {
        super(entityRepository, entityMapper);
    }

}
