package dz.blida.exercise.relationnel.services.columnUniqueKey;


import dz.blida.exercise.relationnel.models.ColumnUniqueKey;
import dz.blida.exercise.shared.generic.basic.GenericService;

public interface ColumnUniqueKeyService extends GenericService<ColumnUniqueKey, String> {

}
