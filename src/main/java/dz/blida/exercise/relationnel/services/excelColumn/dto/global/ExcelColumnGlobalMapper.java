package dz.blida.exercise.relationnel.services.excelColumn.dto.global;

import dz.blida.exercise.relationnel.models.ExcelColumn;
import dz.blida.exercise.relationnel.services.excelColumn.dto.basic.ExcelColumnBasicDto;
import dz.blida.exercise.shared.generic.dto.global.GenericGlobalDtoMapper;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface ExcelColumnGlobalMapper extends GenericGlobalDtoMapper<ExcelColumn, ExcelColumnBasicDto, ExcelColumnGlobalDto> {

}
