package dz.blida.exercise.relationnel.services.tableValidation.dto.models.basic;

import dz.blida.exercise.relationnel.generic.dto.basic.GenericBasicDtoServiceImpl;
import dz.blida.exercise.relationnel.models.TableValidation;
import dz.blida.exercise.relationnel.repostirories.TableValidationRepository;
import org.springframework.stereotype.Service;


@Service
public class TableValidationBasicDtoServiceImpl extends GenericBasicDtoServiceImpl<TableValidation, TableValidationBasicDto, TableValidationRepository, TableValidationBasicMapper, String>
        implements TableValidationBasicDtoService {

    public TableValidationBasicDtoServiceImpl(TableValidationRepository entityRepository, TableValidationBasicMapper entityMapper) {
        super(entityRepository, entityMapper);
    }


}
