package dz.blida.exercise.relationnel.services.columnValidation;

import dz.blida.exercise.relationnel.generic.GenericServiceImpl;
import dz.blida.exercise.relationnel.models.ColumnValidation;
import dz.blida.exercise.relationnel.repostirories.ColumnValidationRepository;
import org.springframework.stereotype.Service;


@Service
public class ColumnValidationServiceImpl extends GenericServiceImpl<ColumnValidation, ColumnValidationRepository, String> implements ColumnValidationService {

    public ColumnValidationServiceImpl(ColumnValidationRepository entityRepository) {
        super(entityRepository);
    }

}
