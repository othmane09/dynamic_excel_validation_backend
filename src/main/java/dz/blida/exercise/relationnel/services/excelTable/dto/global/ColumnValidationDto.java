package dz.blida.exercise.relationnel.services.excelTable.dto.global;

import dz.blida.exercise.relationnel.models.ColumnValidationType;
import lombok.Data;

@Data
public class ColumnValidationDto {

    private String id;

    private String name;

    private ColumnValidationType type;

    private String pattern;

    private String description;

}
