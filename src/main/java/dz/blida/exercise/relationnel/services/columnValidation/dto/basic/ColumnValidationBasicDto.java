package dz.blida.exercise.relationnel.services.columnValidation.dto.basic;

import dz.blida.exercise.relationnel.models.ColumnValidationType;
import lombok.Data;

@Data
public class ColumnValidationBasicDto {

    private String id;

    private String name;

    private ColumnValidationType type;

    private String pattern;

    private String description;

}
