package dz.blida.exercise.relationnel.services.excelColumn.dto.basic;

import dz.blida.exercise.relationnel.models.ExcelColumn;
import dz.blida.exercise.shared.generic.dto.basic.GenericBasicDtoMapper;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface ExcelColumnBasicMapper extends GenericBasicDtoMapper<ExcelColumn, ExcelColumnBasicDto> {

}
