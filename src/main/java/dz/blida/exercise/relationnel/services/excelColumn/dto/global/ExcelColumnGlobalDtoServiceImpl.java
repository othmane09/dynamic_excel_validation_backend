package dz.blida.exercise.relationnel.services.excelColumn.dto.global;


import dz.blida.exercise.relationnel.generic.dto.global.GenericGlobalDtoServiceImpl;
import dz.blida.exercise.relationnel.models.ExcelColumn;
import dz.blida.exercise.relationnel.repostirories.ExcelColumnRepository;
import dz.blida.exercise.relationnel.services.excelColumn.dto.basic.ExcelColumnBasicDto;
import dz.blida.exercise.shared.exeptions.ResponseException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class ExcelColumnGlobalDtoServiceImpl extends GenericGlobalDtoServiceImpl<ExcelColumn, ExcelColumnBasicDto, ExcelColumnGlobalDto, ExcelColumnRepository, ExcelColumnGlobalMapper, String>
        implements ExcelColumnGlobalDtoService {

    public ExcelColumnGlobalDtoServiceImpl(ExcelColumnRepository entityRepository, ExcelColumnGlobalMapper entityMapper) {
        super(entityRepository, entityMapper);
    }

    @Override
    public List<ExcelColumnBasicDto> findByExcelTable_IdAsBasicElseThrowException(String id) {
        if (id == null) throw new ResponseException("id is null  ");
        Optional<List<ExcelColumn>> tableFields = getEntityRepository().findByExcelTable_IdOrderByPosition(id);
        if (tableFields.isPresent())
            return getEntityMapper().entitiesToBasicDtos(tableFields.get());
        throw new ResponseException("no record found");
    }

    @Override
    public List<ExcelColumnGlobalDto> findByExcelTable_IdAsGlobalElseThrowException(String id) {
        if (id == null) throw new ResponseException("id is null  ");
        Optional<List<ExcelColumn>> tableFields = getEntityRepository().findByExcelTable_IdOrderByPosition(id);
        if (tableFields.isPresent())
            return getEntityMapper().entitiesToGlobalDtos(tableFields.get());
        throw new ResponseException("no record found");
    }

}
