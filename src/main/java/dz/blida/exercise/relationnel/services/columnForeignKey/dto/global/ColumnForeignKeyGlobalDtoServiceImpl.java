package dz.blida.exercise.relationnel.services.columnForeignKey.dto.global;

import dz.blida.exercise.relationnel.generic.dto.global.GenericGlobalDtoServiceImpl;
import dz.blida.exercise.relationnel.models.ColumnForeignKey;
import dz.blida.exercise.relationnel.repostirories.ColumnForeignKeyRepository;
import dz.blida.exercise.relationnel.services.columnForeignKey.dto.basic.ColumnForeignKeyBasicDto;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ColumnForeignKeyGlobalDtoServiceImpl extends GenericGlobalDtoServiceImpl<ColumnForeignKey, ColumnForeignKeyBasicDto, ColumnForeignKeyGlobalDto, ColumnForeignKeyRepository, ColumnForeignKeyGlobalMapper, String>
        implements ColumnForeignKeyGlobalDtoService {

    public ColumnForeignKeyGlobalDtoServiceImpl(ColumnForeignKeyRepository entityRepository, ColumnForeignKeyGlobalMapper entityMapper) {
        super(entityRepository, entityMapper);
    }

    @Override
    public List<ColumnForeignKeyGlobalDto> findByExcelTableName(String tableName) {
        return getEntityMapper().entitiesToGlobalDtos(getEntityRepository().findByExcelTableName(tableName));
    }
}
