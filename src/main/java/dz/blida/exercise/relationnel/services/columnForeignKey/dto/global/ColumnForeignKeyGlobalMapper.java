package dz.blida.exercise.relationnel.services.columnForeignKey.dto.global;

import dz.blida.exercise.relationnel.models.ColumnForeignKey;
import dz.blida.exercise.relationnel.services.columnForeignKey.dto.basic.ColumnForeignKeyBasicDto;
import dz.blida.exercise.shared.generic.dto.global.GenericGlobalDtoMapper;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface ColumnForeignKeyGlobalMapper extends GenericGlobalDtoMapper<ColumnForeignKey, ColumnForeignKeyBasicDto, ColumnForeignKeyGlobalDto> {

}
