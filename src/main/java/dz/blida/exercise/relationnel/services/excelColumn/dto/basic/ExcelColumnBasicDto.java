package dz.blida.exercise.relationnel.services.excelColumn.dto.basic;

import lombok.Data;

@Data
public class ExcelColumnBasicDto {

    private String id;

    private String name;

    private int position;

}
