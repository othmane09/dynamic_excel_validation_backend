package dz.blida.exercise.relationnel.services.columnValidation.dto.global;


import dz.blida.exercise.relationnel.models.ColumnValidation;
import dz.blida.exercise.relationnel.services.columnValidation.dto.basic.ColumnValidationBasicDto;
import dz.blida.exercise.shared.generic.dto.global.GenericGlobalDtoService;

import java.util.List;

public interface ColumnValidationGlobalDtoService extends GenericGlobalDtoService<ColumnValidation, ColumnValidationBasicDto, ColumnValidationGlobalDto, String> {

    List<ColumnValidation> findByNameIn(List<String> names);

    ColumnValidation findByName(String name);

    Boolean existsByName(String name);
}