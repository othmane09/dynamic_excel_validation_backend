package dz.blida.exercise.relationnel.services.columnValidation.dto.global;

import dz.blida.exercise.relationnel.models.ColumnValidation;
import dz.blida.exercise.relationnel.services.columnValidation.dto.basic.ColumnValidationBasicDto;
import dz.blida.exercise.shared.generic.dto.global.GenericGlobalDtoMapper;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface ColumnValidationGlobalMapper extends GenericGlobalDtoMapper<ColumnValidation, ColumnValidationBasicDto, ColumnValidationGlobalDto> {

}
