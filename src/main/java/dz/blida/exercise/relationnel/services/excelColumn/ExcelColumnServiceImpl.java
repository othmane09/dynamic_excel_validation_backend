package dz.blida.exercise.relationnel.services.excelColumn;

import dz.blida.exercise.relationnel.generic.GenericServiceImpl;
import dz.blida.exercise.relationnel.models.ExcelColumn;
import dz.blida.exercise.relationnel.repostirories.ExcelColumnRepository;
import org.springframework.stereotype.Service;


@Service
public class ExcelColumnServiceImpl extends GenericServiceImpl<ExcelColumn, ExcelColumnRepository, String> implements ExcelColumnService {

    public ExcelColumnServiceImpl(ExcelColumnRepository entityRepository) {
        super(entityRepository);
    }

}
