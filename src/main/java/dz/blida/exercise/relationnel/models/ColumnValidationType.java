package dz.blida.exercise.relationnel.models;

public enum ColumnValidationType {
    REGEX_PATTERN, REGEX_DATE, NONE
}
