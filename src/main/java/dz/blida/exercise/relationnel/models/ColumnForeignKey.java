package dz.blida.exercise.relationnel.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "column_foreign_key", schema = "excel")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class ColumnForeignKey {

    @Id
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "uuid2")
    @Column(length = 36, nullable = false, updatable = false)
    private String id;

    @Size(min = 1,
            max = 50,
            message = "The  '${validatedValue}' must be between {min} and {max} characters long")
    private String tableName;

    @Size(min = 1,
            max = 50,
            message = "The  '${validatedValue}' must be between {min} and {max} characters long")
    private String tableReferenceColumn;

    @Size(min = 1,
            max = 50,
            message = "The  '${validatedValue}' must be between {min} and {max} characters long")
    private String tableIdColumn;

    @Size(min = 1,
            max = 50,
            message = "The  '${validatedValue}' must be between {min} and {max} characters long")
    private String schema;

    @OneToOne()
    @JsonIgnore
    private ExcelColumn excelColumn;

    public ColumnForeignKey(String tableName,
                            String tableReferenceColumn,
                            String tableIdColumn,
                            String schema) {
        this.tableName = tableName;
        this.tableReferenceColumn = tableReferenceColumn;
        this.tableIdColumn = tableIdColumn;
        this.schema = schema;
    }
}