package dz.blida.exercise.relationnel.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "excel_column", schema = "excel")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class ExcelColumn {

    @Id
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "uuid2")
    @Column(length = 36, nullable = false, updatable = false)
    private String id;

    @Size(min = 2,
            max = 50,
            message = "The  '${validatedValue}' must be between {min} and {max} characters long")
    private String name;

    @Column(columnDefinition = "TEXT")
    private String description;

    private Boolean required;

    private Integer position;

    @JoinColumn(name = "excel_table_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JsonIgnore
    private ExcelTable excelTable;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "excel_columns_column_validations",
            joinColumns = @JoinColumn(name = "excel_column_id"),
            schema = "excel",
            inverseJoinColumns = @JoinColumn(name = "column_validation_id")
    )
    private List<ColumnValidation> columnValidations;

    @OneToOne(cascade = CascadeType.REMOVE)
    private ColumnForeignKey columnForeignKey;

    @OneToOne(cascade = CascadeType.REMOVE)
    private ColumnUniqueKey columnUniqueKey;

}
