package dz.blida.exercise.relationnel.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "table_validation", schema = "excel")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class TableValidation {

    @Id
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "uuid2")
    @Column(length = 36, nullable = false, updatable = false)
    private String id;

    @Size(min = 2,
            max = 50,
            message = "The  '${validatedValue}' must be between {min} and {max} characters long")
    private String name;

    @Column(columnDefinition = "TEXT")
    private String description;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "tableValidations")
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    private List<ExcelTable> excelTables;

    public TableValidation(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
