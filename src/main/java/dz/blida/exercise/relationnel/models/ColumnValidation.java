package dz.blida.exercise.relationnel.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "column_validation", schema = "excel")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ColumnValidation {

    @Id
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "uuid2")
    @Column(length = 36, nullable = false, updatable = false)
    private String id;

    @Size(min = 2,
            max = 50,
            message = "The '${validatedValue}' must be between {min} and {max} characters long")
    private String name;

    @Enumerated(EnumType.STRING)
    private ColumnValidationType type;

    private String pattern;

    @Column(columnDefinition = "TEXT")
    private String description;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "columnValidations")
    private List<ExcelColumn> excelColumns;

    public ColumnValidation(String name, ColumnValidationType type, String pattern, String description) {
        this.type = type;
        this.pattern = pattern;
        this.name = name;
        this.description = description;
    }
}