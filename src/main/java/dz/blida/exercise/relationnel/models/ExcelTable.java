package dz.blida.exercise.relationnel.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "excel_table", schema = "excel")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class ExcelTable {

    @Id
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "uuid2")
    @Column(length = 36, nullable = false, updatable = false)
    private String id;

    @Size(min = 2,
            max = 50,
            message = "The  '${validatedValue}' must be between {min} and {max} characters long")
    @Column(unique = true)
    private String name;

    @Size(max = 250,
            message = "The  '${validatedValue}' must be between {min} and {max} characters long")
    @Column()
    private String url;


    @Column(name = "external_server_insert", columnDefinition = "boolean default false")
    private boolean externalServerInsert = false;

    @Column(columnDefinition = "TEXT")
    private String description;

    @ManyToMany(fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    @JoinTable(name = "excel_tables_table_validations",
            joinColumns = @JoinColumn(name = "excel_table_id"), schema = "excel",
            inverseJoinColumns = @JoinColumn(name = "table_validation_id")
    )
    private List<TableValidation> tableValidations;

    @OneToMany(mappedBy = "excelTable", fetch = FetchType.LAZY)
    private Set<ExcelColumn> excelColumns;

    public ExcelTable(String name, String description, String url) {
        this.url = url;
        this.name = name;
        this.description = description;
    }

    public ExcelTable(String name, String description, String url, List<TableValidation> tableValidations) {
        this(name, description, url);
        this.tableValidations = tableValidations;
    }
}
