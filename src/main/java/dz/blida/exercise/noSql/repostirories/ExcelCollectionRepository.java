package dz.blida.exercise.noSql.repostirories;

import dz.blida.exercise.noSql.models.EntityId;
import dz.blida.exercise.noSql.models.ExcelCollection;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ExcelCollectionRepository extends MongoRepository<ExcelCollection, String> {

    @Query(value = "{ name : ?0 }", fields = "{ _id : 1 }")
    Optional<EntityId> findIdByName(String name);

    Boolean existsByName(String name);

    Optional<ExcelCollection> findByName(String name);

}
