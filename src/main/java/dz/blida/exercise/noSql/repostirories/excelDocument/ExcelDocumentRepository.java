package dz.blida.exercise.noSql.repostirories.excelDocument;

import dz.blida.exercise.noSql.models.ExcelCollection;
import dz.blida.exercise.noSql.models.ExcelDocument;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ExcelDocumentRepository extends MongoRepository<ExcelDocument, String>, CustomExcelDocumentRepository {

    Optional<List<ExcelDocument>> findByExcelCollection_Id(String id);

    Optional<List<ExcelDocument>> findAllByExcelCollection_Id(String id);

    Optional<List<ExcelDocument>> findAllByIdIn(List<String> ids);

    Optional<List<ExcelDocument>> findByContentsLike(String s);

    Optional<List<ExcelDocument>> findByExcelCollectionIsNotIn(List<ExcelCollection> excelCollections);
}
