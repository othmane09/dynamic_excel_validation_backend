package dz.blida.exercise.noSql.repostirories;

import dz.blida.exercise.noSql.models.ExcelCollection;
import dz.blida.exercise.noSql.models.ExcelField;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ExcelFieldRepository extends MongoRepository<ExcelField, String> {

    Optional<ExcelField> findById(String id);

    Optional<List<ExcelField>> findByExcelCollection_IdOrderByPosition(String id);

    List<ExcelField> findByExcelCollectionAndIdNotIn(ExcelCollection excelCollection, List<String> ids);

    Optional<List<ExcelField>> findByExcelCollectionIsNotIn(List<ExcelCollection> excelCollections);


}
