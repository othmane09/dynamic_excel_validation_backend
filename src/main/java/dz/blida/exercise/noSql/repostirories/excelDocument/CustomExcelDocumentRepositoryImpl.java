package dz.blida.exercise.noSql.repostirories.excelDocument;

import dz.blida.exercise.noSql.models.ExcelDocument;
import dz.blida.exercise.shared.models.PageInfo;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class CustomExcelDocumentRepositoryImpl implements CustomExcelDocumentRepository {
    @Autowired
    MongoTemplate mongoTemplate;

    public List<ExcelDocument> getUniqueKeyValueByExcelCollectionId(String id, String uniqueKey) {

        Criteria criteria = Criteria.where("excelCollection.$id").is(new ObjectId(id));
        Query query = new Query();
        query.addCriteria(criteria);
        query.fields().include("contents." + uniqueKey).exclude("id");

        return mongoTemplate.find(query, ExcelDocument.class);
    }

    public Page<ExcelDocument> getExcelDocumentByExcelCollectionIdWithFilter(String id, PageInfo pageInfo) {

        Pageable pageable = PageRequest.of(pageInfo.getPage(), pageInfo.getSize());
        Query query = new Query();
        Criteria criteria1 = Criteria.where("excelCollection.$id").is(new ObjectId(id));
        query.addCriteria(criteria1);

        if (pageInfo.getFilter() != null)
            pageInfo.getFilter().forEach((key, value) -> {

                query.addCriteria(Criteria.where("contents." + key).regex(value, "i"));
            });

        List<ExcelDocument> excelDocuments = mongoTemplate.find(query.with(pageable), ExcelDocument.class);

        Page<ExcelDocument> patientPage = PageableExecutionUtils.getPage(
                excelDocuments,
                pageable,
                () -> mongoTemplate.count(query.skip(0).limit(0), ExcelDocument.class));

        return patientPage;
    }


}


