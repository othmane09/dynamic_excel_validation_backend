package dz.blida.exercise.noSql.repostirories.excelDocument;

import dz.blida.exercise.noSql.models.ExcelDocument;
import dz.blida.exercise.shared.models.PageInfo;
import org.springframework.data.domain.Page;

import java.util.List;

public interface CustomExcelDocumentRepository {

    List<ExcelDocument> getUniqueKeyValueByExcelCollectionId(String id, String uniqueKey);

    Page<ExcelDocument> getExcelDocumentByExcelCollectionIdWithFilter(String id, PageInfo pageInfo);

}


