package dz.blida.exercise.noSql.services.excelCollection.dto.global;

import dz.blida.exercise.noSql.models.EntityId;
import dz.blida.exercise.noSql.models.ExcelCollection;
import dz.blida.exercise.noSql.services.excelCollection.dto.basic.ExcelCollectionBasicDto;
import dz.blida.exercise.shared.generic.dto.global.GenericGlobalDtoService;

public interface ExcelCollectionGlobalDtoService extends GenericGlobalDtoService<ExcelCollection, ExcelCollectionBasicDto, ExcelCollectionGlobalDto, String> {

    ExcelCollection findByName(String name);

    ExcelCollection findByNameElseThrowException(String name);

    Boolean existsByName(String name);

    EntityId findIdByName(String name);

    EntityId findIdByNameElseThrowException(String name);
}