package dz.blida.exercise.noSql.services.excelCollection.dto.global;

import dz.blida.exercise.noSql.generic.dto.global.GenericGlobalDtoServiceImpl;
import dz.blida.exercise.noSql.models.EntityId;
import dz.blida.exercise.noSql.models.ExcelCollection;
import dz.blida.exercise.noSql.repostirories.ExcelCollectionRepository;
import dz.blida.exercise.noSql.services.excelCollection.dto.basic.ExcelCollectionBasicDto;
import dz.blida.exercise.shared.exeptions.ResponseException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ExcelCollectionGlobalDtoServiceImpl extends GenericGlobalDtoServiceImpl<ExcelCollection, ExcelCollectionBasicDto, ExcelCollectionGlobalDto, ExcelCollectionRepository, ExcelCollectionGlobalMapper, String>
        implements ExcelCollectionGlobalDtoService {


    public ExcelCollectionGlobalDtoServiceImpl(ExcelCollectionRepository entityRepository, ExcelCollectionGlobalMapper entityMapper) {
        super(entityRepository, entityMapper);
    }

    @Override
    public ExcelCollection findByName(String name) {
        if (name == null) return null;
        Optional<ExcelCollection> excelCollection = getEntityRepository().findByName(name);
        return excelCollection.orElse(null);
    }

    @Override
    public ExcelCollection findByNameElseThrowException(String name) {
        if (name == null)
            throw new ResponseException("name is Null Please try to enter some value next time");
        Optional<ExcelCollection> excelCollection = getEntityRepository().findByName(name);
        if (excelCollection.isPresent())
            return excelCollection.get();
        throw new ResponseException("there is no collection with the name :" + name);
    }

    @Override
    public Boolean existsByName(String name) {
        return getEntityRepository().existsByName(name);
    }

    @Override
    public EntityId findIdByName(String name) {
        if (name == null) return null;
        Optional<EntityId> id = getEntityRepository().findIdByName(name);
        return id.orElse(null);
    }

    @Override
    public EntityId findIdByNameElseThrowException(String name) {
        if (name == null)
            throw new ResponseException("name is Null Please try to enter some value next time");
        Optional<EntityId> id = getEntityRepository().findIdByName(name);
        if (id.isPresent())
            return id.get();
        throw new ResponseException("Excel " + name + " does not exist ");
    }
}
