package dz.blida.exercise.noSql.services.excelCollection.dto.basic;

import dz.blida.exercise.noSql.models.ExcelCollection;
import dz.blida.exercise.shared.generic.dto.basic.GenericBasicDtoMapper;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface ExcelCollectionBasicMapper extends GenericBasicDtoMapper<ExcelCollection, ExcelCollectionBasicDto> {
}
