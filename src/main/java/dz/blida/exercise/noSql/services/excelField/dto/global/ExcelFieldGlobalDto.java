package dz.blida.exercise.noSql.services.excelField.dto.global;

import dz.blida.exercise.noSql.models.ExcelCollection;
import lombok.Data;

@Data
public class ExcelFieldGlobalDto {

    private String id;

    private String name;

    private int position;

    private ExcelCollection excelCollection;


}
