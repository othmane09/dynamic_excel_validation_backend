package dz.blida.exercise.noSql.services.excelDocument.dto.basic;

import dz.blida.exercise.noSql.models.ExcelDocument;
import dz.blida.exercise.shared.generic.dto.basic.GenericBasicDtoService;

public interface ExcelDocumentBasicDtoService extends GenericBasicDtoService<ExcelDocument, ExcelDocumentBasicDto, String> {

}