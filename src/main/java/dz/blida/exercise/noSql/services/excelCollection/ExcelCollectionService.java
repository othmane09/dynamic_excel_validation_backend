package dz.blida.exercise.noSql.services.excelCollection;

import dz.blida.exercise.noSql.models.ExcelCollection;
import dz.blida.exercise.shared.generic.basic.GenericService;

public interface ExcelCollectionService extends GenericService<ExcelCollection, String> {


}
