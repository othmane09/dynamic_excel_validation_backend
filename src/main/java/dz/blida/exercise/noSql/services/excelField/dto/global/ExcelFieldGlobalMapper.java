package dz.blida.exercise.noSql.services.excelField.dto.global;

import dz.blida.exercise.noSql.models.ExcelField;
import dz.blida.exercise.noSql.services.excelField.dto.basic.ExcelFieldBasicDto;
import dz.blida.exercise.shared.generic.dto.global.GenericGlobalDtoMapper;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface ExcelFieldGlobalMapper extends GenericGlobalDtoMapper<ExcelField, ExcelFieldBasicDto, ExcelFieldGlobalDto> {

}
