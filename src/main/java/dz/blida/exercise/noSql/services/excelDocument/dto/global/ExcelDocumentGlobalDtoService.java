package dz.blida.exercise.noSql.services.excelDocument.dto.global;

import dz.blida.exercise.noSql.models.ExcelCollection;
import dz.blida.exercise.noSql.models.ExcelDocument;
import dz.blida.exercise.noSql.services.excelDocument.dto.basic.ExcelDocumentBasicDto;
import dz.blida.exercise.shared.generic.dto.global.GenericGlobalDtoService;
import dz.blida.exercise.shared.models.PageInfo;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

public interface ExcelDocumentGlobalDtoService extends GenericGlobalDtoService<ExcelDocument, ExcelDocumentBasicDto, ExcelDocumentGlobalDto, String> {

    List<ExcelDocumentGlobalDto> findByExcelCollection_IdElseThrowException(String collectionId);

    List<ExcelDocument> findAllByExcelCollection_Id(String collectionId);

    List<ExcelDocument> findAllByIdInElseThrowException(List<String> ids);

    void updateValidation(ExcelDocumentGlobalDto excelDocumentGlobalDto);

    List<ExcelDocument> getUniqueKeyValueByExcelCollectionId(String excelCollectionId, String uniqueKeyName);

    Page<ExcelDocument> getExcelDocumentByExcelCollectionIdWithFilter(String id, PageInfo pageInfo);

    void deleteByIds(List<String> ids);

    Optional<List<ExcelDocument>> findByExcelCollectionIsNotIn(List<ExcelCollection> excelCollections);
}
