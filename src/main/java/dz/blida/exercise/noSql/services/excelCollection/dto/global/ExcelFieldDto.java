package dz.blida.exercise.noSql.services.excelCollection.dto.global;

import lombok.Data;

@Data
public class ExcelFieldDto {

    private String id;

    private String name;

    private int position;
}
