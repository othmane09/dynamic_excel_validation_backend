package dz.blida.exercise.noSql.services.excelField.dto.global;

import lombok.Data;

import java.util.Map;

@Data
public class ExcelDocumentDto {

    private String id;

    private Map<String, String> contents;

}
