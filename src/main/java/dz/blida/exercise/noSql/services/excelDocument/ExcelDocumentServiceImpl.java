package dz.blida.exercise.noSql.services.excelDocument;

import dz.blida.exercise.noSql.generic.GenericServiceImpl;
import dz.blida.exercise.noSql.models.ExcelDocument;
import dz.blida.exercise.noSql.repostirories.excelDocument.ExcelDocumentRepository;
import org.springframework.stereotype.Service;


@Service
public class ExcelDocumentServiceImpl extends GenericServiceImpl<ExcelDocument, ExcelDocumentRepository, String> implements ExcelDocumentService {

    public ExcelDocumentServiceImpl(ExcelDocumentRepository entityRepository) {
        super(entityRepository);
    }

}
