package dz.blida.exercise.noSql.services.excelCollection.dto.global;

import lombok.Data;

import java.util.List;

@Data
public class ExcelCollectionGlobalDto {

    private String id;

    private String name;

    private List<ExcelFieldDto> excelFields;

    private List<ExcelDocumentDto> excelDocuments;
}
