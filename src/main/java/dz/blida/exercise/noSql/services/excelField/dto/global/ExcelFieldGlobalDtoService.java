package dz.blida.exercise.noSql.services.excelField.dto.global;

import dz.blida.exercise.noSql.models.ExcelCollection;
import dz.blida.exercise.noSql.models.ExcelField;
import dz.blida.exercise.noSql.services.excelField.dto.basic.ExcelFieldBasicDto;
import dz.blida.exercise.shared.generic.dto.global.GenericGlobalDtoService;

import java.util.List;
import java.util.Optional;

public interface ExcelFieldGlobalDtoService extends GenericGlobalDtoService<ExcelField, ExcelFieldBasicDto, ExcelFieldGlobalDto, String> {

    List<ExcelFieldBasicDto> findByExcelCollection_IdElseThrowException(String excelName);

    List<ExcelField> findByExcelCollection_Id(String excelCollectionId);

    List<ExcelField> findByExcelCollectionAndIdNotIn(ExcelCollection excelCollection, List<String> ids);

    Optional<List<ExcelField>> findByExcelCollectionIsNotIn(List<ExcelCollection> excelCollections);
}
