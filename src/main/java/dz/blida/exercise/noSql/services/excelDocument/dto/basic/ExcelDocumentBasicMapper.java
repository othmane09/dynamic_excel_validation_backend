package dz.blida.exercise.noSql.services.excelDocument.dto.basic;

import dz.blida.exercise.noSql.models.ExcelDocument;
import dz.blida.exercise.shared.generic.dto.basic.GenericBasicDtoMapper;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface ExcelDocumentBasicMapper extends GenericBasicDtoMapper<ExcelDocument, ExcelDocumentBasicDto> {


}
