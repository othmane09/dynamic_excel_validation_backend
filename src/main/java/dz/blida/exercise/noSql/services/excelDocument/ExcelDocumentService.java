package dz.blida.exercise.noSql.services.excelDocument;

import dz.blida.exercise.noSql.models.ExcelDocument;
import dz.blida.exercise.shared.generic.basic.GenericService;


public interface ExcelDocumentService extends GenericService<ExcelDocument, String> {

}
