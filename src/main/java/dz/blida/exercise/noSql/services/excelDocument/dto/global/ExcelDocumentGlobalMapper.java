package dz.blida.exercise.noSql.services.excelDocument.dto.global;

import dz.blida.exercise.noSql.models.ExcelDocument;
import dz.blida.exercise.noSql.services.excelDocument.dto.basic.ExcelDocumentBasicDto;
import dz.blida.exercise.shared.generic.dto.global.GenericGlobalDtoMapper;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface ExcelDocumentGlobalMapper extends GenericGlobalDtoMapper<ExcelDocument, ExcelDocumentBasicDto, ExcelDocumentGlobalDto> {


}
