package dz.blida.exercise.noSql.services.excelField.dto.basic;

import dz.blida.exercise.noSql.models.ExcelField;
import dz.blida.exercise.shared.generic.dto.basic.GenericBasicDtoMapper;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface ExcelFieldBasicMapper extends GenericBasicDtoMapper<ExcelField, ExcelFieldBasicDto> {

}
