package dz.blida.exercise.noSql.services.excelField;

import dz.blida.exercise.noSql.generic.GenericServiceImpl;
import dz.blida.exercise.noSql.models.ExcelField;
import dz.blida.exercise.noSql.repostirories.ExcelFieldRepository;
import org.springframework.stereotype.Service;


@Service
public class ExcelFieldServiceImpl extends GenericServiceImpl<ExcelField, ExcelFieldRepository, String> implements ExcelFieldService {

    public ExcelFieldServiceImpl(ExcelFieldRepository entityRepository) {
        super(entityRepository);
    }

}
