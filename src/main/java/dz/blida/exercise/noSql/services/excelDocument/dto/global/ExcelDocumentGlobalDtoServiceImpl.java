package dz.blida.exercise.noSql.services.excelDocument.dto.global;

import dz.blida.exercise.noSql.generic.dto.global.GenericGlobalDtoServiceImpl;
import dz.blida.exercise.noSql.models.ExcelCollection;
import dz.blida.exercise.noSql.models.ExcelDocument;
import dz.blida.exercise.noSql.repostirories.excelDocument.ExcelDocumentRepository;
import dz.blida.exercise.noSql.services.excelDocument.dto.basic.ExcelDocumentBasicDto;
import dz.blida.exercise.shared.exeptions.ResponseException;
import dz.blida.exercise.shared.models.PageInfo;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class ExcelDocumentGlobalDtoServiceImpl extends GenericGlobalDtoServiceImpl<ExcelDocument, ExcelDocumentBasicDto, ExcelDocumentGlobalDto, ExcelDocumentRepository, ExcelDocumentGlobalMapper, String>
        implements ExcelDocumentGlobalDtoService {

    public ExcelDocumentGlobalDtoServiceImpl(ExcelDocumentRepository entityRepository, ExcelDocumentGlobalMapper entityMapper) {
        super(entityRepository, entityMapper);
    }

    @Override
    public List<ExcelDocumentGlobalDto> findByExcelCollection_IdElseThrowException(String collectionId) {
        if (collectionId == null) throw new ResponseException("collectionId is null  ");
        Optional<List<ExcelDocument>> excelDocuments = getEntityRepository().findByExcelCollection_Id(collectionId);
        if (excelDocuments.isPresent())
            return getEntityMapper().entitiesToGlobalDtos(excelDocuments.get());
        throw new ResponseException("no record found");
    }

    @Override
    public List<ExcelDocument> findAllByExcelCollection_Id(String collectionId) {
        if (collectionId == null) throw new ResponseException("collectionId is null  ");
        Optional<List<ExcelDocument>> excelDocuments = getEntityRepository().findAllByExcelCollection_Id(collectionId);
        return excelDocuments.orElse(null);
    }

    @Override
    public List<ExcelDocument> findAllByIdInElseThrowException(List<String> ids) {
        if (ids == null)
            throw new ResponseException("ids is null try again later");
        Optional<List<ExcelDocument>> excelDocuments = getEntityRepository().findAllByIdIn(ids);
        if (excelDocuments.isPresent())
            return excelDocuments.get();
        throw new ResponseException("no record found");
    }

    @Override
    public void updateValidation(ExcelDocumentGlobalDto excelDocumentGlobalDto) {
        if (excelDocumentGlobalDto.getContents() == null)
            throw new ResponseException("excel document required  some content");
    }

    @Override
    public List<ExcelDocument> getUniqueKeyValueByExcelCollectionId(String excelCollectionId, String uniqueKeyName) {
        return getEntityRepository().getUniqueKeyValueByExcelCollectionId(excelCollectionId, uniqueKeyName);
    }

    @Override
    public Page<ExcelDocument> getExcelDocumentByExcelCollectionIdWithFilter(String id, PageInfo pageInfo) {
        if (id == null)
            throw new ResponseException("id is null try again later");
        return getEntityRepository().getExcelDocumentByExcelCollectionIdWithFilter(id, pageInfo);
    }

    @Override
    public void deleteByIds(List<String> ids) {
        List<ExcelDocument> excelDocuments = findAllByIdInElseThrowException(ids);
        try {
            getEntityRepository().deleteAll(excelDocuments);
        } catch (Exception e) {
            throw new ResponseException("This Documents Cant Be Deleted Contact Admin For More Information");
        }
    }

    @Override
    public Optional<List<ExcelDocument>> findByExcelCollectionIsNotIn(List<ExcelCollection> excelCollections) {
        return getEntityRepository().findByExcelCollectionIsNotIn(excelCollections);
    }
}
