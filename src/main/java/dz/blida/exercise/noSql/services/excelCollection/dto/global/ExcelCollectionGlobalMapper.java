package dz.blida.exercise.noSql.services.excelCollection.dto.global;

import dz.blida.exercise.noSql.models.ExcelCollection;
import dz.blida.exercise.noSql.services.excelCollection.dto.basic.ExcelCollectionBasicDto;
import dz.blida.exercise.shared.generic.dto.global.GenericGlobalDtoMapper;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface ExcelCollectionGlobalMapper extends GenericGlobalDtoMapper<ExcelCollection, ExcelCollectionBasicDto, ExcelCollectionGlobalDto> {
}
