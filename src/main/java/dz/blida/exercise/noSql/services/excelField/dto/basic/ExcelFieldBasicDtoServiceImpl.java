package dz.blida.exercise.noSql.services.excelField.dto.basic;

import dz.blida.exercise.noSql.generic.dto.basic.GenericBasicDtoServiceImpl;
import dz.blida.exercise.noSql.models.ExcelField;
import dz.blida.exercise.noSql.repostirories.ExcelFieldRepository;
import org.springframework.stereotype.Service;


@Service
public class ExcelFieldBasicDtoServiceImpl extends GenericBasicDtoServiceImpl<ExcelField, ExcelFieldBasicDto, ExcelFieldRepository, ExcelFieldBasicMapper, String>
        implements ExcelFieldBasicDtoService {

    public ExcelFieldBasicDtoServiceImpl(ExcelFieldRepository entityRepository, ExcelFieldBasicMapper entityMapper) {
        super(entityRepository, entityMapper);
    }


}
