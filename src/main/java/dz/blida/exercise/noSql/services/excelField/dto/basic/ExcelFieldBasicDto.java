package dz.blida.exercise.noSql.services.excelField.dto.basic;

import lombok.Data;

@Data
public class ExcelFieldBasicDto {

    private String id;

    private String name;

    private int position;

}
