package dz.blida.exercise.noSql.services.excelCollection.dto.basic;

import dz.blida.exercise.noSql.models.ExcelCollection;
import dz.blida.exercise.shared.generic.dto.basic.GenericBasicDtoService;

public interface ExcelCollectionBasicDtoService extends GenericBasicDtoService<ExcelCollection, ExcelCollectionBasicDto, String> {

}