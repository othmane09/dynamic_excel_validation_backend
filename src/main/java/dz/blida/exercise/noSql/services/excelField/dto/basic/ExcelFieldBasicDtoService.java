package dz.blida.exercise.noSql.services.excelField.dto.basic;

import dz.blida.exercise.noSql.models.ExcelField;
import dz.blida.exercise.shared.generic.dto.basic.GenericBasicDtoService;

public interface ExcelFieldBasicDtoService extends GenericBasicDtoService<ExcelField, ExcelFieldBasicDto, String> {

}