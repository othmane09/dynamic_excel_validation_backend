package dz.blida.exercise.noSql.services.excelDocument.dto.global;

import lombok.Data;

import java.util.Map;

@Data
public class ExcelDocumentGlobalDto {

    private String id;

    private Map<String, String> contents;

    private ExcelCollectionDto excelCollection;
}
