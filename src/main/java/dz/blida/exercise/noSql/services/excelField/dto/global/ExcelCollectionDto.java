package dz.blida.exercise.noSql.services.excelField.dto.global;

import lombok.Data;

import java.util.List;

@Data
public class ExcelCollectionDto {

    private String id;

    private String name;

    private List<ExcelDocumentDto> excelDocuments;

}
