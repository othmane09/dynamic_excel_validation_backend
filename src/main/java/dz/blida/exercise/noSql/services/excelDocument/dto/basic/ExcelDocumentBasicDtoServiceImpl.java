package dz.blida.exercise.noSql.services.excelDocument.dto.basic;

import dz.blida.exercise.noSql.generic.dto.basic.GenericBasicDtoServiceImpl;
import dz.blida.exercise.noSql.models.ExcelDocument;
import dz.blida.exercise.noSql.repostirories.excelDocument.ExcelDocumentRepository;
import org.springframework.stereotype.Service;


@Service
public class ExcelDocumentBasicDtoServiceImpl extends GenericBasicDtoServiceImpl<ExcelDocument, ExcelDocumentBasicDto, ExcelDocumentRepository, ExcelDocumentBasicMapper, String>
        implements ExcelDocumentBasicDtoService {

    public ExcelDocumentBasicDtoServiceImpl(ExcelDocumentRepository entityRepository, ExcelDocumentBasicMapper entityMapper) {
        super(entityRepository, entityMapper);
    }

}
