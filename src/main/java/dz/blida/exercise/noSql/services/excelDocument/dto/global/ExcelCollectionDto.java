package dz.blida.exercise.noSql.services.excelDocument.dto.global;

import lombok.Data;

import java.util.List;

@Data
public class ExcelCollectionDto {

    private String id;

    private String name;

    private List<ExcelFieldDto> excelFields;

}
