package dz.blida.exercise.noSql.services.excelField;

import dz.blida.exercise.noSql.models.ExcelField;
import dz.blida.exercise.shared.generic.basic.GenericService;


public interface ExcelFieldService extends GenericService<ExcelField, String> {
}
