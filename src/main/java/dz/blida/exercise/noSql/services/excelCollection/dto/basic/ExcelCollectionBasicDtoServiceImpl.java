package dz.blida.exercise.noSql.services.excelCollection.dto.basic;

import dz.blida.exercise.noSql.generic.dto.basic.GenericBasicDtoServiceImpl;
import dz.blida.exercise.noSql.models.ExcelCollection;
import dz.blida.exercise.noSql.repostirories.ExcelCollectionRepository;
import org.springframework.stereotype.Service;


@Service
public class ExcelCollectionBasicDtoServiceImpl extends GenericBasicDtoServiceImpl<ExcelCollection, ExcelCollectionBasicDto, ExcelCollectionRepository, ExcelCollectionBasicMapper, String>
        implements ExcelCollectionBasicDtoService {

    public ExcelCollectionBasicDtoServiceImpl(ExcelCollectionRepository entityRepository, ExcelCollectionBasicMapper entityMapper) {
        super(entityRepository, entityMapper);
    }

}
