package dz.blida.exercise.noSql.services.excelDocument.dto.basic;

import lombok.Data;

import java.util.Map;

@Data
public class ExcelDocumentBasicDto {

    private String id;

    private Map<String, String> contents;
}
