package dz.blida.exercise.noSql.services.excelCollection;

import dz.blida.exercise.noSql.generic.GenericServiceImpl;
import dz.blida.exercise.noSql.models.ExcelCollection;
import dz.blida.exercise.noSql.repostirories.ExcelCollectionRepository;
import org.springframework.stereotype.Service;

@Service
public class ExcelCollectionServiceImpl extends GenericServiceImpl<ExcelCollection, ExcelCollectionRepository, String> implements ExcelCollectionService {

    public ExcelCollectionServiceImpl(ExcelCollectionRepository entityRepository) {
        super(entityRepository);
    }

}
