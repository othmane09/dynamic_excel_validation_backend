package dz.blida.exercise.noSql.services.excelCollection.dto.basic;

import lombok.Data;

@Data
public class ExcelCollectionBasicDto {

    private String id;

    private String name;
}
