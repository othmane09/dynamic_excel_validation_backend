package dz.blida.exercise.noSql.services.excelField.dto.global;

import dz.blida.exercise.noSql.generic.dto.global.GenericGlobalDtoServiceImpl;
import dz.blida.exercise.noSql.models.ExcelCollection;
import dz.blida.exercise.noSql.models.ExcelField;
import dz.blida.exercise.noSql.repostirories.ExcelFieldRepository;
import dz.blida.exercise.noSql.services.excelField.dto.basic.ExcelFieldBasicDto;
import dz.blida.exercise.shared.exeptions.ResponseException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class ExcelFieldGlobalDtoServiceImpl extends GenericGlobalDtoServiceImpl<ExcelField, ExcelFieldBasicDto, ExcelFieldGlobalDto, ExcelFieldRepository, ExcelFieldGlobalMapper, String>
        implements ExcelFieldGlobalDtoService {

    public ExcelFieldGlobalDtoServiceImpl(ExcelFieldRepository entityRepository, ExcelFieldGlobalMapper entityMapper) {
        super(entityRepository, entityMapper);
    }

    @Override
    public List<ExcelFieldBasicDto> findByExcelCollection_IdElseThrowException(String excelCollectionId) {
        if (excelCollectionId == null)
            throw new ResponseException("excelCollectionId  is null try to enter valid one");
        Optional<List<ExcelField>> headers = getEntityRepository().findByExcelCollection_IdOrderByPosition(excelCollectionId);
        if (headers.isPresent())
            return getEntityMapper().entitiesToBasicDtos(headers.get());
        throw new ResponseException("there is no fields with this excel name ");
    }

    @Override
    public List<ExcelField> findByExcelCollection_Id(String excelCollectionId) {
        if (excelCollectionId == null)
            return null;
        Optional<List<ExcelField>> headers = getEntityRepository().findByExcelCollection_IdOrderByPosition(excelCollectionId);
        return headers.orElse(null);
    }

    @Override
    public List<ExcelField> findByExcelCollectionAndIdNotIn(ExcelCollection excelCollection, List<String> ids) {
        return getEntityRepository().findByExcelCollectionAndIdNotIn(excelCollection, ids);
    }

    @Override
    public Optional<List<ExcelField>> findByExcelCollectionIsNotIn(List<ExcelCollection> excelCollections) {
        return getEntityRepository().findByExcelCollectionIsNotIn(excelCollections);
    }
}
