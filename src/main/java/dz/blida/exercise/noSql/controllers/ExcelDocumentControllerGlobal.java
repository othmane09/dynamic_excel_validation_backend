package dz.blida.exercise.noSql.controllers;

import dz.blida.exercise.noSql.models.ExcelDocument;
import dz.blida.exercise.noSql.services.excelDocument.dto.basic.ExcelDocumentBasicDto;
import dz.blida.exercise.noSql.services.excelDocument.dto.global.ExcelDocumentGlobalDto;
import dz.blida.exercise.noSql.services.excelDocument.dto.global.ExcelDocumentGlobalDtoService;
import dz.blida.exercise.shared.generic.dto.global.GenericGlobalDtoController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/excel_documents")
public class ExcelDocumentControllerGlobal extends GenericGlobalDtoController<ExcelDocument, ExcelDocumentBasicDto, ExcelDocumentGlobalDto, ExcelDocumentGlobalDtoService, String> {

    public ExcelDocumentControllerGlobal(ExcelDocumentGlobalDtoService entityService) {
        super(entityService);
    }

}
