package dz.blida.exercise.noSql.controllers;

import dz.blida.exercise.noSql.models.ExcelField;
import dz.blida.exercise.noSql.services.excelField.dto.basic.ExcelFieldBasicDtoService;
import dz.blida.exercise.noSql.services.excelField.dto.global.ExcelFieldGlobalDto;
import dz.blida.exercise.shared.generic.dto.basic.GenericBasicDtoController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/excel_fields")
public class ExcelFieldControllerGlobal extends GenericBasicDtoController<ExcelField, ExcelFieldGlobalDto, ExcelFieldBasicDtoService, String> {

    public ExcelFieldControllerGlobal(ExcelFieldBasicDtoService entityService) {
        super(entityService);
    }
}
