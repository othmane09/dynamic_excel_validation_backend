package dz.blida.exercise.noSql.controllers;

import dz.blida.exercise.noSql.models.ExcelCollection;
import dz.blida.exercise.noSql.services.excelCollection.dto.basic.ExcelCollectionBasicDtoService;
import dz.blida.exercise.noSql.services.excelCollection.dto.global.ExcelCollectionGlobalDto;
import dz.blida.exercise.shared.generic.dto.basic.GenericBasicDtoController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/excel_collections")
public class ExcelCollectionControllerGlobal extends GenericBasicDtoController<ExcelCollection, ExcelCollectionGlobalDto, ExcelCollectionBasicDtoService, String> {

    public ExcelCollectionControllerGlobal(ExcelCollectionBasicDtoService entityService) {
        super(entityService);
    }
}
