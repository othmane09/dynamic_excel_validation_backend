package dz.blida.exercise.noSql.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Map;

@Getter
@Setter
@Document(collection = "excel_documents")
@AllArgsConstructor
@NoArgsConstructor
public class ExcelDocument {

    @Id
    private String id;

    @DBRef
    @JsonIgnore
    private ExcelCollection excelCollection;

    @Field(name = "contents")
    private Map<String, String> contents;

    public ExcelDocument(Map<String, String> contents, ExcelCollection excelCollection) {
        this.contents = contents;
        this.excelCollection = excelCollection;
    }
}
