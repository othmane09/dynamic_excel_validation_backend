package dz.blida.exercise.noSql.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Getter
@Setter
@Document(collection = "excel_fields")
@AllArgsConstructor
@NoArgsConstructor
public class ExcelField {

    @Id
    private String id;

    @Field(name = "name")
    private String name;

    @Field(name = "position")
    private int position;

    @DBRef
    @JsonIgnore
    private ExcelCollection excelCollection;

    public ExcelField(String name, int position, ExcelCollection excelCollection) {
        this.name = name;
        this.position = position;
        this.excelCollection = excelCollection;
    }

}
