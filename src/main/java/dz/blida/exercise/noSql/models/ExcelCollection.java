package dz.blida.exercise.noSql.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

@Getter
@Setter
@Document(collection = "excel_collections")
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class ExcelCollection {

    @Id
    private String id;

    @Field(name = "name")
    private String name;

    @Field(name = "status")
    private Status status;

    @DBRef
    private List<ExcelField> excelFields;

    @DBRef
    private List<ExcelDocument> excelDocuments;
}
