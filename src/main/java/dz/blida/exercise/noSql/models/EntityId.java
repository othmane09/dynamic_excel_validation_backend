package dz.blida.exercise.noSql.models;

import org.springframework.data.annotation.Id;

public class EntityId {

    @Id
    private String id;

    public String getValue() {
        return id;
    }
}
