package dz.blida.exercise.noSql.models;

public enum Status {
    IMPORTATION,
    EDIT_STRUCTURE,
    EDIT_CONTENT
}
