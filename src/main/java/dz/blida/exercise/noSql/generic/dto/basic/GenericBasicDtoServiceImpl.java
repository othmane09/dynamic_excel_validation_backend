package dz.blida.exercise.noSql.generic.dto.basic;

import dz.blida.exercise.noSql.generic.GenericServiceImpl;
import dz.blida.exercise.shared.exeptions.ResponseException;
import dz.blida.exercise.shared.generic.dto.basic.GenericBasicDtoMapper;
import dz.blida.exercise.shared.generic.dto.basic.GenericBasicDtoService;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public class GenericBasicDtoServiceImpl<ENTITY, SIMPLE_DTO, REPO extends MongoRepository, MAP extends GenericBasicDtoMapper, ID>
        extends GenericServiceImpl<ENTITY, REPO, ID>
        implements GenericBasicDtoService<ENTITY, SIMPLE_DTO, ID> {

    MAP entityMapper;

    public GenericBasicDtoServiceImpl(REPO entityRepository, MAP entityMapper) {
        super(entityRepository);
        this.entityMapper = entityMapper;
    }

    @Override
    public SIMPLE_DTO findByIdAsBasicDto(ID id) {
        if (id == null) return null;
        Optional<ENTITY> entityRecord = getEntityRepository().findById(id);
        if (entityRecord.isPresent())
            return (SIMPLE_DTO) this.entityMapper.entityToBasicDto(entityRecord.get());
        return null;
    }

    @Override
    public SIMPLE_DTO findByIdAsBasicDtoElseThrowException(ID id) {
        if (id == null)
            throw new ResponseException("Id is Null Please try to enter some value next time");
        Optional<ENTITY> entityRecord = getEntityRepository().findById(id);
        if (entityRecord.isPresent())
            return (SIMPLE_DTO) this.entityMapper.entityToBasicDto(entityRecord.get());
        throw new ResponseException("No Recode Match With This Id ??");
    }

    @Override
    public List<SIMPLE_DTO> findAllAsBasicDtos() {
        return this.entityMapper.entitiesToBasicDtos(getEntityRepository().findAll());
    }

    @Override
    public List<SIMPLE_DTO> findAllAsBasicDtosElseThrowException() {
        List<ENTITY> entityRecords = getEntityRepository().findAll();
        if (entityRecords == null)
            throw new ResponseException("The Is No Record Of This Entity");
        return this.entityMapper.entitiesToBasicDtos(entityRecords);
    }

    @Override
    public SIMPLE_DTO saveFromBasicDto(SIMPLE_DTO entity) {
        return (SIMPLE_DTO) this.entityMapper.entityToBasicDto(getEntityRepository().save(this.entityMapper.basicDtoToEntity(entity)));
    }

    @Override
    public List<SIMPLE_DTO> saveAllFromBasicDtos(Iterable<SIMPLE_DTO> entities) {
        return (List<SIMPLE_DTO>) this.entityMapper.basicDtoToEntity(getEntityRepository().saveAll(this.entityMapper.basicDtosToEntities((List) entities)));
    }

    @Override
    public void deleteFromBasicDto(SIMPLE_DTO entity) {
        try {
            getEntityRepository().delete(this.entityMapper.basicDtoToEntity(entity));
        } catch (Exception e) {
            throw new ResponseException("This Records Cant Be Deleted Contact Admin For More Information");
        }
    }

    @Override
    public void deleteAllFromBasicDto(Iterable<SIMPLE_DTO> entities) {
        try {
            getEntityRepository().deleteAll(this.entityMapper.basicDtosToEntities((List) entities));
        } catch (Exception e) {
            throw new ResponseException("This Records Cant Be Deleted Contact Admin For More Information");
        }
    }

    public MAP getEntityMapper() {
        return this.entityMapper;
    }

}
