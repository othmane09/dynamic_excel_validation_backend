package dz.blida.exercise.shared.services.excelWizard.importFile;

import dz.blida.exercise.noSql.models.ExcelCollection;
import dz.blida.exercise.noSql.models.ExcelDocument;
import dz.blida.exercise.noSql.models.ExcelField;
import dz.blida.exercise.noSql.models.Status;
import dz.blida.exercise.noSql.services.excelCollection.dto.global.ExcelCollectionGlobalDtoService;
import dz.blida.exercise.noSql.services.excelDocument.dto.global.ExcelDocumentGlobalDtoService;
import dz.blida.exercise.noSql.services.excelField.dto.global.ExcelFieldGlobalDtoService;
import dz.blida.exercise.relationnel.services.excelTable.dto.global.ExcelTableGlobalDtoService;
import dz.blida.exercise.shared.exeptions.ResponseException;
import dz.blida.exercise.shared.services.excelWizard.excelStructure.ExcelStructureService;
import lombok.AllArgsConstructor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

@Service
@AllArgsConstructor
public class ImportFileServiceImpl implements ImportFileService {

    ExcelCollectionGlobalDtoService excelCollectionDtoService;

    ExcelTableGlobalDtoService excelTableDtoService;

    ExcelFieldGlobalDtoService excelFieldDtoService;

    ExcelDocumentGlobalDtoService excelDocumentDtoService;

    ExcelStructureService excelStructureService;

    @Override
    public void importExcelFile(MultipartFile file, String name) throws IOException {


        XSSFSheet worksheet = convertExcelFileToWorkSheet(file);
        XSSFRow excelHeader = worksheet.getRow(0);
        ExcelCollection excelCollection = initEmptyExcelCollection(name);
        List<ExcelField> excelFields = excelFieldDtoService.saveAll(getExcelFields(excelHeader, excelCollection));
        excelCollection.setExcelFields(excelFields);
        List<ExcelDocument> excelDocuments = excelDocumentDtoService.saveAll(convertExcelFileRowsToListOfExcelDocuments(worksheet, excelFields, excelCollection));
        if (excelDocuments.isEmpty()) {
            excelStructureService.deleteExcelFile(name);
            throw new ResponseException("excel have no content , or format not supported try again with different excel");
        }
        excelCollection.setExcelDocuments(excelDocuments);
        excelCollection = excelCollectionDtoService.save(excelCollection);
        if (excelStructureService.checkExcelColumnsEqualToExcelFields(name)) {
            excelCollection.setStatus(Status.EDIT_CONTENT);
            excelCollectionDtoService.save(excelCollection);
        }
    }

    @Override
    public XSSFSheet convertExcelFileToWorkSheet(MultipartFile file) throws IOException {
        try {
            XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
            return workbook.getSheetAt(0);
        } catch (Exception e) {
            throw new ResponseException("Sorry this format not supported  ,contact admin for more information");
        }
    }

    @Override
    public Map<String, String> convertExcelFileRowToMapOfString(XSSFRow excelRow, List<ExcelField> excelFields) {
        Map<String, String> list = new HashMap<>();
        for (int i = 0; i < excelFields.size(); i++)
            list.put(excelFields.get(i).getName(), getCellValueAsString(excelRow.getCell(i)));
        return list;
    }

    @Override
    public List<ExcelDocument> convertExcelFileRowsToListOfExcelDocuments(XSSFSheet excelContent, List<ExcelField> excelFields, ExcelCollection excelCollection) {
        List<ExcelDocument> excelDocument = new ArrayList<>();
        for (int index = 1; index < excelContent.getPhysicalNumberOfRows(); index++) {
            XSSFRow row = excelContent.getRow(index);
            if (row != null) {
                Map<String, String> tempContent = convertExcelFileRowToMapOfString(row, excelFields);
                if (!tempContent.isEmpty())
                    excelDocument.add(new ExcelDocument(tempContent, excelCollection));
            }

        }
        return excelDocument;
    }

    @Override
    public List<ExcelField> convertExcelFileRowsToListOfExcelFields(XSSFRow excelRow, ExcelCollection excelCollection) {
        List<ExcelField> excelFields = new ArrayList<>();
        for (int i = 0; i < excelRow.getPhysicalNumberOfCells(); i++)
            excelFields.add(new ExcelField(getCellValueAsString(excelRow.getCell(i)), i, excelCollection));
        return excelFields;
    }

    @Override
    public String getCellValueAsString(Cell cell) {
        BigDecimal zero = new BigDecimal("0.0");
        if (cell == null)
            return "";
        switch (cell.getCellType()) {
            case NUMERIC:
                BigDecimal bigDecimal = new BigDecimal(String.valueOf(cell.getNumericCellValue()));
                int  intValue = bigDecimal.intValue();
                return (bigDecimal.subtract(new BigDecimal(intValue)).equals(zero)) ? String.valueOf(intValue) : bigDecimal.toPlainString();
            case STRING:
                return cell.getStringCellValue();
            case ERROR:
                return String.valueOf(cell.getErrorCellValue());
            case BLANK:
            case _NONE:
                return "";
            case FORMULA:
                return cell.getCellFormula();
            case BOOLEAN:
                return String.valueOf(cell.getBooleanCellValue());
        }
        return "";
    }

    @Override
    public void excelValidation(String name) {
        if (!excelTableDtoService.existsByName(name))
            throw new ResponseException("Sorry There is no support for this table  ,contact admin for more information");
        if (excelCollectionDtoService.existsByName(name))
            throw new ResponseException("Sorry There is an existing temporary excel collection  , Delete the old one to be able to continue with this procedure");
    }

    private ExcelCollection initEmptyExcelCollection(String name) {
        ExcelCollection excelCollection = new ExcelCollection().builder()
                .name(name)
                .status(Status.EDIT_STRUCTURE)
                .build();
        return excelCollectionDtoService.save(excelCollection);
    }

    private List<ExcelField> getExcelFields(XSSFRow excelHeader, ExcelCollection excelCollection) {
        List<ExcelField> excelFields = convertExcelFileRowsToListOfExcelFields(excelHeader, excelCollection);
        excelFields.sort(Comparator.comparing(ExcelField::getPosition));
        return excelFields;
    }

}
