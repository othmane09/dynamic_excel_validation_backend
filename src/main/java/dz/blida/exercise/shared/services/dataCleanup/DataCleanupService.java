package dz.blida.exercise.shared.services.dataCleanup;

import dz.blida.exercise.noSql.models.ExcelCollection;

import java.util.List;

public interface DataCleanupService {

    void clearAllDataWithNoReference();

    void deleteAllExcelFieldWithNoReference(List<ExcelCollection> excelCollections);

    void deleteAllExcelDocumentWithNoReference(List<ExcelCollection> excelCollections);
}
