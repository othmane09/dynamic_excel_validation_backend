package dz.blida.exercise.shared.services.excelWizard.excelStructure;

import dz.blida.exercise.noSql.models.Status;
import dz.blida.exercise.noSql.services.excelField.dto.global.ExcelFieldGlobalDto;

import java.util.List;
import java.util.Map;

public interface ExcelStructureService {

    Map<String, Object> getExcelHeaderStructure(String excelName);

    void updateExcelStructure(String excelName, List<ExcelFieldGlobalDto> tempExcelColumns);

    Status getExcelFileStatus(String name);

    boolean checkExcelColumnsEqualToExcelFields(String excelName);

    void structureValidation(String name);

    void deleteExcelFile(String excelName);

    void deleteExcelFileWithFieldsAndDocuments(String excelName);

}
