package dz.blida.exercise.shared.services.excelWizard.excelInsertValidData;

import dz.blida.exercise.noSql.services.excelDocument.dto.global.ExcelDocumentGlobalDtoService;
import dz.blida.exercise.relationnel.services.columnForeignKey.dto.global.ColumnForeignKeyGlobalDtoService;
import dz.blida.exercise.relationnel.services.excelTable.dto.global.ColumnValidationDto;
import dz.blida.exercise.relationnel.services.excelTable.dto.global.ExcelTableGlobalDto;
import dz.blida.exercise.relationnel.services.excelTable.dto.global.ExcelTableGlobalDtoService;
import dz.blida.exercise.shared.exeptions.ResponseException;
import dz.blida.exercise.shared.models.ExcelDocumentError;
import dz.blida.exercise.shared.models.ValidData;
import dz.blida.exercise.shared.services.excelWizard.excelData.ExcelDataService;
import dz.blida.exercise.shared.services.excelWizard.excelStructure.ExcelStructureService;
import dz.blida.exercise.shared.services.excelWizard.excelValidation.ExcelValidationService;
import lombok.AllArgsConstructor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ExcelInsertValidDataServiceImpl implements ExcelInsertValidDataService {

    private final WebClient webClient;
    ExcelTableGlobalDtoService excelTableGlobalDtoService;
    ExcelDocumentGlobalDtoService documentGlobalDtoService;
    ExcelValidationService excelValidationService;
    ColumnForeignKeyGlobalDtoService columnForeignKeyService;
    ExcelStructureService excelStructureService;
    ExcelDataService excelDataService;

    @Override
    public Mono<?> insertData(String excelName, String action) {

        ExcelTableGlobalDto excelTableGlobalDto = excelTableGlobalDtoService.findAllByNameElseThrowException(excelName);

        List<ExcelDocumentError> dataValidation = excelValidationService.validateAllExcelDocumentByExcelName(excelName).getBody();

        List<ValidData> validData = getAllValidData(dataValidation);

        List<Map<String, String>> valid_contents = getContentsOfValidData(validData, excelTableGlobalDto);

        if (valid_contents.isEmpty())
            throw new ResponseException("There is no valid data try again later ");

        List<String> valid_ids = getIdsOfValidData(validData);

        boolean dropExcelFile = (dataValidation.size() == validData.size());

        if (Objects.equals(action, "DOWNLOAD"))
            return downloadValidDataToExternalClient(excelTableGlobalDto, valid_contents, valid_ids, dropExcelFile);
        return insertValidDataToExternalClient(excelTableGlobalDto, valid_contents, valid_ids, dropExcelFile);
    }

    @Override
    public Mono<String> insertValidDataToExternalClient(ExcelTableGlobalDto excelTableGlobalDto, List<Map<String, String>> valid_contents, List<String> valid_ids, boolean dropExcelFile) {

        return webClient.post()
                .uri(excelTableGlobalDto.getUrl())
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(valid_contents))
                .retrieve()
                .onStatus(HttpStatus::is2xxSuccessful, response -> {
                    documentGlobalDtoService.deleteByIds(valid_ids);
                    if (dropExcelFile)
                        excelStructureService.deleteExcelFile(excelTableGlobalDto.getName());
                    return Mono.empty();
                })
                .onStatus(HttpStatus::isError, response -> {
                    if (response.statusCode() == HttpStatus.NOT_FOUND) {
                        return Mono.error(new ResponseException("http request  " + excelTableGlobalDto.getUrl() + " NOT FOUND : 404"));
                    }
                    return Mono.error(new ResponseException("http request error :" + response.statusCode()));
                })
                .bodyToMono(String.class);
    }

    @Override
    public Mono<ResponseEntity<Resource>> downloadValidDataToExternalClient(ExcelTableGlobalDto excelTableGlobalDto, List<Map<String, String>> valid_contents, List<String> valid_ids, boolean dropExcelFile) {

        StringBuilder fileContent = new StringBuilder("INSERT INTO ").append(excelTableGlobalDto.getName()).append(" ( ");

        getTableHeader(excelTableGlobalDto, fileContent);
        getTableContent(excelTableGlobalDto, valid_contents, fileContent);
        String result = fileContent.toString();
        ByteArrayResource resource = new ByteArrayResource(result.getBytes());
        documentGlobalDtoService.deleteByIds(valid_ids);
        if (dropExcelFile)
            excelStructureService.deleteExcelFile(excelTableGlobalDto.getName());
        return Mono.fromSupplier(() -> ResponseEntity.ok()
                .contentLength(result.getBytes().length)
                .contentType(MediaType.TEXT_PLAIN)
                .header("Content-Disposition", "attachment; filename=\"file.txt\"")
                .body(resource));
    }

    private StringBuilder getTableHeader(ExcelTableGlobalDto excelTable, StringBuilder fileContent) {
        excelTable.getExcelColumns().forEach((value) -> {
            fileContent.append(value.getName()).append(",");
        });
        fileContent.deleteCharAt(fileContent.length() - 1);
        fileContent.append(") VALUES");
        return fileContent;
    }

    private StringBuilder getTableContent(ExcelTableGlobalDto excelTable, List<Map<String, String>> validData, StringBuilder fileContent) {
        validData.forEach((dataV) -> {
            fileContent.append("(");
            excelTable.getExcelColumns().forEach((column) -> {
                Optional<ColumnValidationDto> columnValidation = column.getColumnValidations().stream().findFirst();
                if (columnValidation.isPresent() && columnValidation.get().getName().equals("NUMBER"))
                    fileContent.append(dataV.get(column.getName())).append(",");
                else
                    fileContent.append("'").append(dataV.get(column.getName())).append("'").append(",");
            });
            fileContent.deleteCharAt(fileContent.length() - 1);
            fileContent.append("),");
        });
        fileContent.deleteCharAt(fileContent.length() - 1);
        fileContent.append(";");
        return fileContent;
    }

    private List<ValidData> getAllValidData(List<ExcelDocumentError> dataValidation) {
        return dataValidation
                .parallelStream()
                .filter(excelDocumentError -> !excelDocumentError.isHaveValidationError())
                .map(excelDocumentError -> new ValidData(excelDocumentError.getId(), excelDocumentError.getContents()))
                .collect(Collectors.toList());
    }

    private List<Map<String, String>> getContentsOfValidData(List<ValidData> validData, ExcelTableGlobalDto excelTableGlobalDto) {
        return validData.parallelStream()
                .map(data -> getValueOfContent(data.getContents(), excelTableGlobalDto))
                .collect(Collectors.toList());
    }

    private Map<String, String> getValueOfContent(Map<String, String> content, ExcelTableGlobalDto excelTableGlobalDto) {
        excelTableGlobalDto.getExcelColumns().forEach(column -> {
            String objectValue = content.get(column.getName());
            if (column.getColumnForeignKey() != null) {
                objectValue = excelDataService.getForeignKeyIdValue(column.getColumnForeignKey().getTableName(),
                        column.getColumnForeignKey().getSchema(),
                        column.getColumnForeignKey().getTableReferenceColumn(),
                        column.getColumnForeignKey().getTableIdColumn(),
                        content.get(column.getName()));
            }
            content.put(column.getName(), objectValue);
        });
        return content;
    }

    private List<String> getIdsOfValidData(List<ValidData> validData) {
        return validData
                .parallelStream()
                .map(ValidData::getId)
                .collect(Collectors.toList());
    }
}
