package dz.blida.exercise.shared.services.excel;

import dz.blida.exercise.shared.models.datatableMetadata.DatabaseMetadata;

import java.util.List;

public interface DatabaseMetadataService {

    DatabaseMetadata getDatabaseStructure();

    List<String> getAllTableNamesBySchema(String schema);

    List<String> getAllSchemas();

    List<String> getAllColumnsBySchemaAndTable(String schema, String tableName);
}
