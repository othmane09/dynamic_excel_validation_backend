package dz.blida.exercise.shared.services.excelWizard.excelInsertValidData;


import dz.blida.exercise.relationnel.services.excelTable.dto.global.ExcelTableGlobalDto;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;

public interface ExcelInsertValidDataService {

    Mono<?> insertData(String excelName, String action);

    Mono<String> insertValidDataToExternalClient(ExcelTableGlobalDto excelTableGlobalDto, List<Map<String, String>> valid_contents, List<String> valid_ids, boolean dropExcelFile);

    Mono<ResponseEntity<Resource>> downloadValidDataToExternalClient(ExcelTableGlobalDto excelTableGlobalDto, List<Map<String, String>> valid_contents, List<String> valid_ids, boolean dropExcelFile);
}
