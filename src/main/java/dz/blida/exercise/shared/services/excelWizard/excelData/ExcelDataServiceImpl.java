package dz.blida.exercise.shared.services.excelWizard.excelData;

import dz.blida.exercise.noSql.models.ExcelDocument;
import dz.blida.exercise.noSql.services.excelCollection.dto.global.ExcelCollectionGlobalDtoService;
import dz.blida.exercise.noSql.services.excelDocument.dto.global.ExcelDocumentGlobalDtoService;
import dz.blida.exercise.relationnel.services.columnForeignKey.dto.global.ColumnForeignKeyGlobalDto;
import dz.blida.exercise.relationnel.services.columnForeignKey.dto.global.ColumnForeignKeyGlobalDtoService;
import dz.blida.exercise.relationnel.services.columnUniqueKey.dto.global.ColumnUniqueKeyGlobalDto;
import dz.blida.exercise.relationnel.services.columnUniqueKey.dto.global.ColumnUniqueKeyGlobalDtoService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@AllArgsConstructor
@Service
public class ExcelDataServiceImpl implements ExcelDataService {

    EntityManager entityManager;

    ColumnForeignKeyGlobalDtoService columnForeignKeyDtoService;

    ColumnUniqueKeyGlobalDtoService columnUniqueKeyGlobalDtoService;

    ExcelDocumentGlobalDtoService excelDocumentGlobalDtoService;

    ExcelCollectionGlobalDtoService excelCollectionGlobalDtoService;

    @Override
    public List<String> getListByTableName(String tableName, String schema, String tableReferenceColumn) {
        String sql = "SELECT CAST (" + tableReferenceColumn + " AS TEXT) FROM " + schema + "." + tableName;
        Query query = entityManager.createNativeQuery(sql);
        return query.getResultList();
    }

    @Override
    public String getForeignKeyIdValue(String tableName, String schema, String tableReferenceColumn, String tableIdColumn, String value) {
        String sql = "SELECT CAST (" + tableIdColumn + " AS TEXT) FROM " + schema + "." + tableName + " where CAST (" + tableReferenceColumn + " AS TEXT )  = CAST ('" + value + "' AS TEXT ) limit 1";
        Query query = entityManager.createNativeQuery(sql);
        return (String) query.getSingleResult();
    }

    @Override
    public Map<String, List<String>> getAllForeignKeyValues(String tableName) {
        Map<String, List<String>> result = new HashMap<>();
        List<ColumnForeignKeyGlobalDto> columnForeignKeyGlobalDto = columnForeignKeyDtoService.findByExcelTableName(tableName);
        columnForeignKeyGlobalDto.forEach((e) -> {
            result.put(e.getExcelColumn().getName(), this.getListByTableName(e.getTableName(), e.getSchema(), e.getTableReferenceColumn()));
        });
        return result;
    }

    @Override
    public Map<String, List<String>> getAllUniqueKeyValues(String tableName) {
        Map<String, List<String>> result = new HashMap<>();
        List<ColumnUniqueKeyGlobalDto> columnUniqueKeyGlobalDtos = columnUniqueKeyGlobalDtoService.findByExcelTableName(tableName);
        columnUniqueKeyGlobalDtos.forEach((e) -> {
            result.put(e.getExcelColumn().getName(), this.getListByTableName(e.getTableName(), e.getSchema(), e.getTableReferenceColumn()));
        });
        return result;
    }

    @Override
    public Map<String, List<String>> getAllExcelFileUniqueKeyValues(String tableName) {
        String excelCollectionId = excelCollectionGlobalDtoService.findIdByName(tableName).getValue();
        Map<String, List<String>> result = new HashMap<>();
        List<ColumnUniqueKeyGlobalDto> columnUniqueKeyGlobalDtos = columnUniqueKeyGlobalDtoService.findByExcelTableName(tableName);
        columnUniqueKeyGlobalDtos.forEach((e) -> {
            var temp = excelDocumentGlobalDtoService.getUniqueKeyValueByExcelCollectionId(excelCollectionId, e.getExcelColumn().getName());
            result.put(e.getExcelColumn().getName(),
                    excelDocumentGlobalDtoService.getUniqueKeyValueByExcelCollectionId(excelCollectionId, e.getExcelColumn().getName()).stream()
                            .map(excelDocument -> convertJsonToStringValue(excelDocument, e.getExcelColumn().getName()))
                            .collect(Collectors.toList()));
        });
        return result;
    }

    private String convertJsonToStringValue(ExcelDocument excelDocument, String key) {
        return excelDocument.getContents().get(key);
    }
}
