package dz.blida.exercise.shared.services.excelWizard.excelStructure;

import dz.blida.exercise.noSql.models.ExcelCollection;
import dz.blida.exercise.noSql.models.ExcelDocument;
import dz.blida.exercise.noSql.models.ExcelField;
import dz.blida.exercise.noSql.models.Status;
import dz.blida.exercise.noSql.services.excelCollection.dto.global.ExcelCollectionGlobalDtoService;
import dz.blida.exercise.noSql.services.excelDocument.dto.global.ExcelDocumentGlobalDto;
import dz.blida.exercise.noSql.services.excelDocument.dto.global.ExcelDocumentGlobalDtoService;
import dz.blida.exercise.noSql.services.excelField.dto.basic.ExcelFieldBasicDto;
import dz.blida.exercise.noSql.services.excelField.dto.global.ExcelFieldGlobalDto;
import dz.blida.exercise.noSql.services.excelField.dto.global.ExcelFieldGlobalDtoService;
import dz.blida.exercise.relationnel.services.excelColumn.dto.basic.ExcelColumnBasicDto;
import dz.blida.exercise.relationnel.services.excelColumn.dto.global.ExcelColumnGlobalDtoService;
import dz.blida.exercise.relationnel.services.excelTable.dto.global.ExcelTableGlobalDtoService;
import dz.blida.exercise.shared.exeptions.ResponseException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ExcelStructureServiceImpl implements ExcelStructureService {

    public final String empty_filed = "empty_filed";
    ExcelFieldGlobalDtoService excelFieldDtoService;
    ExcelColumnGlobalDtoService excelColumnDtoService;
    ExcelCollectionGlobalDtoService excelCollectionDtoService;
    ExcelTableGlobalDtoService excelTableDtoService;
    ExcelDocumentGlobalDtoService excelDocumentDtoService;

    @Override
    public Map<String, Object> getExcelHeaderStructure(String excelName) {
        List<ExcelFieldBasicDto> excelFields = excelFieldDtoService.findByExcelCollection_IdElseThrowException(excelCollectionDtoService.findIdByNameElseThrowException(excelName).getValue());
        List<ExcelColumnBasicDto> excelColumns = excelColumnDtoService.findByExcelTable_IdAsBasicElseThrowException(excelTableDtoService.findIdByNameElseThrowException(excelName));
        Map<String, Object> response = new HashMap<>();
        response.put("excelFields", excelFields);
        response.put("excelColumns", excelColumns);
        return response;
    }

    @Override
    public void updateExcelStructure(String excelName, List<ExcelFieldGlobalDto> tempExcelColumns) {
        updateExcelFields(excelName, tempExcelColumns);
        updateExcelDocuments(excelName);
        updateExcelCollectionStatus(excelName);
    }

    @Override
    public boolean checkExcelColumnsEqualToExcelFields(String excelName) {
        List<ExcelFieldBasicDto> excelFields = excelFieldDtoService.findByExcelCollection_IdElseThrowException(excelCollectionDtoService.findIdByNameElseThrowException(excelName).getValue());
        List<ExcelColumnBasicDto> excelColumns = excelColumnDtoService.findByExcelTable_IdAsBasicElseThrowException(excelTableDtoService.findIdByNameElseThrowException(excelName));
        if (excelFields.size() != excelColumns.size())
            return false;
        for (int i = 0; i < excelFields.size(); i++)
            if (!(excelFields.get(i).getName().equals(excelColumns.get(i).getName())) || !(excelFields.get(i).getPosition() == excelColumns.get(i).getPosition()))
                return false;
        return true;
    }

    @Override
    public void structureValidation(String name) {
        Status excelStatus = this.getExcelFileStatus(name);
        if (excelStatus != Status.EDIT_STRUCTURE)
            throw new ResponseException("Current Statue Of This Excel Is " + excelStatus + " You Can t Edit Structure In This Step");
    }

    @Override
    public void deleteExcelFile(String excelName) {
        ExcelCollection excelCollection = excelCollectionDtoService.findByName(excelName);
        if (excelCollection != null)
            excelCollectionDtoService.delete(excelCollection);
    }

    @Override
    public void deleteExcelFileWithFieldsAndDocuments(String excelName) {
        ExcelCollection excelCollection = excelCollectionDtoService.findByName(excelName);
        if (excelCollection != null) {
            List<ExcelDocument> excelDocuments = excelDocumentDtoService.findAllByExcelCollection_Id(excelCollection.getId());
            List<ExcelField> excelFields = excelFieldDtoService.findByExcelCollection_Id(excelCollection.getId());
            excelDocumentDtoService.deleteAll(excelDocuments);
            excelFieldDtoService.deleteAll(excelFields);
            excelCollectionDtoService.delete(excelCollection);
        }
    }

    @Override
    public Status getExcelFileStatus(String name) {
        excelTableDtoService.findIdByNameElseThrowException(name);
        ExcelCollection excelCollection = excelCollectionDtoService.findByName(name);
        if (excelCollection == null)
            return Status.IMPORTATION;
        return excelCollection.getStatus();
    }

    private void updateExcelFields(String excelName, List<ExcelFieldGlobalDto> tempExcelColumns) {
        int excelColumnsSize = excelColumnDtoService.findByExcelTable_IdAsBasicElseThrowException(excelTableDtoService.findIdByNameElseThrowException(excelName)).size();
        int excelFieldsSize = tempExcelColumns.size();
        int j = 0;
        ExcelCollection excelCollection = excelCollectionDtoService.findByNameElseThrowException(excelName);
        List<String> excelHeaderIds = new ArrayList<>();
        for (int i = 0; i < excelColumnsSize && i < excelFieldsSize; i++) {
            excelHeaderIds.add(addOrUpdateExcelField(tempExcelColumns.get(i), excelCollection, i));
            j = i;
        }
        for (int i = j; i < excelColumnsSize - 1; i++)
            excelHeaderIds.add(addEmptyExcelField(excelCollection, i));
        deleteUselessExcelFields(excelCollection, excelHeaderIds);
    }

    private void updateExcelDocuments(String excelName) {
        ExcelCollection excelCollection = excelCollectionDtoService.findByNameElseThrowException(excelName);
        List<ExcelColumnBasicDto> excelColumnsDto = excelColumnDtoService.findByExcelTable_IdAsBasicElseThrowException(excelTableDtoService.findIdByNameElseThrowException(excelName));
        List<ExcelFieldBasicDto> excelFieldsDto = excelFieldDtoService.findByExcelCollection_IdElseThrowException(excelCollectionDtoService.findIdByNameElseThrowException(excelName).getValue());
        Map<String, String> headerMapper = new HashMap<>();
        for (int i = 0; i < excelColumnsDto.size(); i++)
            headerMapper.put(excelColumnsDto.get(i).getName(), excelFieldsDto.get(i).getName());
        List<ExcelDocument> excelDocuments = excelDocumentDtoService.findByExcelCollection_IdElseThrowException(excelCollectionDtoService.findIdByNameElseThrowException(excelName).getValue())
                .stream().map((excelDocument -> updateDocumentContents(excelDocument, headerMapper, excelCollection))).collect(Collectors.toList());
        excelDocumentDtoService.deleteAllFromGlobalDto(excelDocumentDtoService.findByExcelCollection_IdElseThrowException(excelCollection.getId()));
        excelCollection.setExcelDocuments(excelDocumentDtoService.saveAll(excelDocuments));
    }

    private void updateExcelCollectionStatus(String excelName) {
        ExcelCollection excelCollection = excelCollectionDtoService.findByNameElseThrowException(excelName);
        excelCollection.setStatus(Status.EDIT_CONTENT);
        excelCollectionDtoService.save(excelCollection);
    }


    private ExcelDocument updateDocumentContents(ExcelDocumentGlobalDto excelDocument, Map<String, String> headerMapper, ExcelCollection excelCollection) {
        Map<String, String> content = new HashMap<>();
        for (String key : headerMapper.keySet())
            if (headerMapper.get(key).equals(empty_filed))
                content.put(key, "");
            else
                content.put(key, excelDocument.getContents().get(headerMapper.get(key)));
        return new ExcelDocument(content, excelCollection);
    }

    private String addOrUpdateExcelField(ExcelFieldGlobalDto tempExcelField, ExcelCollection excelCollection, int position) {
        ExcelField excelField = excelFieldDtoService.findById(tempExcelField.getId());
        String excelFieldId;
        if (tempExcelField.getId() == null || excelField == null)
            excelFieldId = addEmptyExcelField(excelCollection, position);
        else if (excelField.getPosition() == position)
            excelFieldId = excelField.getId();
        else
            excelFieldId = updateExcelFieldPosition(excelField, position);
        return excelFieldId;
    }

    private String updateExcelFieldPosition(ExcelField excelField, int position) {
        excelField.setPosition(position);
        return excelFieldDtoService.save(excelField).getId();
    }

    private String addEmptyExcelField(ExcelCollection excelCollection, int position) {
        ExcelField excelField = new ExcelField();
        excelField.setName(empty_filed);
        excelField.setPosition(position);
        excelField.setExcelCollection(excelCollection);
        return excelFieldDtoService.save(excelField).getId();
    }

    private void deleteUselessExcelFields(ExcelCollection excelCollection, List<String> ids) {
        excelFieldDtoService.deleteAll(excelFieldDtoService.findByExcelCollectionAndIdNotIn(excelCollection, ids));
    }

}
