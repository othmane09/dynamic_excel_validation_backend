package dz.blida.exercise.shared.services.excel;

import dz.blida.exercise.shared.models.datatableMetadata.Column;
import dz.blida.exercise.shared.models.datatableMetadata.DatabaseMetadata;
import dz.blida.exercise.shared.models.datatableMetadata.Schema;
import dz.blida.exercise.shared.models.datatableMetadata.Table;
import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@AllArgsConstructor
public class DatabaseMetadataServiceImpl implements DatabaseMetadataService {

    private final JdbcTemplate jdbcTemplate;

    @Override
    public DatabaseMetadata getDatabaseStructure() {
        DatabaseMetadata databaseMetadata = new DatabaseMetadata();
        List<String> schemas = getAllSchemas();
        schemas.forEach(
                schema -> {
                    Schema schemaTemp = new Schema();
                    schemaTemp.setSchemaName(schema);
                    List<String> tables = getAllTableNamesBySchema(schema);
                    tables.forEach(
                            table -> {
                                Table tableTemp = new Table();
                                List<Column> columns = new ArrayList<>();
                                tableTemp.setTableName(table);
                                getAllColumnsBySchemaAndTable(schema, table).forEach(column -> {
                                    Column tempColumn = new Column();
                                    tempColumn.setColumnName(column);
                                    columns.add(tempColumn);
                                });
                                tableTemp.setColumns(columns);
                                schemaTemp.addTable(tableTemp);
                            }
                    );
                    databaseMetadata.addSchema(schemaTemp);
                }
        );
        return databaseMetadata;
    }

    @Override
    public List<String> getAllTableNamesBySchema(String schema) {
        return jdbcTemplate.queryForList(
                "SELECT table_name FROM information_schema.tables WHERE table_schema = ?",
                new Object[]{schema},
                String.class);
    }

    @Override
    public List<String> getAllSchemas() {
        List<String> schemas = jdbcTemplate.queryForList(
                "SELECT schema_name FROM information_schema.schemata",
                String.class);
        schemas.removeAll(Arrays.asList("pg_toast", "pg_catalog", "information_schema"));
        return schemas;
    }

    @Override
    public List<String> getAllColumnsBySchemaAndTable(String schema, String tableName) {

        String sql = "SELECT column_name FROM information_schema.columns WHERE table_name = ? AND table_schema = ?";
        List<String> names = jdbcTemplate.queryForList(sql, String.class, tableName, schema);
        return names;
    }
}
