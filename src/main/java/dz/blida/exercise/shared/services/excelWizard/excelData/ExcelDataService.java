package dz.blida.exercise.shared.services.excelWizard.excelData;

import java.util.List;
import java.util.Map;

public interface ExcelDataService {

    List<String> getListByTableName(String tableName, String schema, String idName);

    Map<String, List<String>> getAllForeignKeyValues(String tableName);

    String getForeignKeyIdValue(String tableName, String schema, String tableReferenceColumn, String tableIdColumn, String value);

    Map<String, List<String>> getAllUniqueKeyValues(String tableName);

    Map<String, List<String>> getAllExcelFileUniqueKeyValues(String tableName);


}
