package dz.blida.exercise.shared.services.excelWizard.excelValidation;

import dz.blida.exercise.noSql.services.excelDocument.dto.global.ExcelDocumentGlobalDto;
import dz.blida.exercise.shared.models.ExcelError;
import dz.blida.exercise.shared.models.PageInfo;

import java.util.Map;

public interface ExcelValidationService {

    Map<String, Object> validateExcelDocumentByPage(PageInfo pageInfo);

    ExcelError validateAllExcelDocumentByExcelName(String excelName);

    Map<String, Object> validateExcelDocumentById(String id);

    Map<String, Object> updateExcelDocumentWithValidation(ExcelDocumentGlobalDto excelDocumentGlobalDto);

}
