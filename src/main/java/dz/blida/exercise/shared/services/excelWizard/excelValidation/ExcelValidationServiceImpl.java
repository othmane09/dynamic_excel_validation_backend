package dz.blida.exercise.shared.services.excelWizard.excelValidation;

import dz.blida.exercise.noSql.models.ExcelDocument;
import dz.blida.exercise.noSql.services.excelCollection.dto.global.ExcelCollectionGlobalDtoService;
import dz.blida.exercise.noSql.services.excelDocument.dto.global.ExcelDocumentGlobalDto;
import dz.blida.exercise.noSql.services.excelDocument.dto.global.ExcelDocumentGlobalDtoService;
import dz.blida.exercise.relationnel.services.excelColumn.dto.global.ExcelColumnGlobalDtoService;
import dz.blida.exercise.relationnel.services.excelTable.dto.global.ColumnValidationDto;
import dz.blida.exercise.relationnel.services.excelTable.dto.global.ExcelTableGlobalDto;
import dz.blida.exercise.relationnel.services.excelTable.dto.global.ExcelTableGlobalDtoService;
import dz.blida.exercise.relationnel.services.excelTable.dto.global.TableValidationDto;
import dz.blida.exercise.shared.exeptions.ResponseException;
import dz.blida.exercise.shared.models.ExcelDocumentError;
import dz.blida.exercise.shared.models.ExcelError;
import dz.blida.exercise.shared.models.PageInfo;
import dz.blida.exercise.shared.services.excelWizard.excelData.ExcelDataService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static dz.blida.exercise.shared.utility.StringValidation.isDatePattern;
import static dz.blida.exercise.shared.utility.StringValidation.isRegexPattern;

@Service
@AllArgsConstructor
public class ExcelValidationServiceImpl implements ExcelValidationService {

    ExcelCollectionGlobalDtoService excelCollectionGlobalDtoService;

    ExcelDocumentGlobalDtoService excelDocumentDtoService;

    ExcelColumnGlobalDtoService excelColumnGlobalDtoService;

    ExcelTableGlobalDtoService excelTableGlobalDtoService;

    ExcelDocumentGlobalDtoService excelDocumentGlobalDtoService;


    ExcelDataService excelDataService;

    @Override
    public Map<String, Object> validateExcelDocumentByPage(PageInfo pageInfo) {
        String excelCollectionId = excelCollectionGlobalDtoService.findByNameElseThrowException(pageInfo.getTableName()).getId();
        Page<ExcelDocument> excelDocuments = excelDocumentDtoService.getExcelDocumentByExcelCollectionIdWithFilter(excelCollectionId, pageInfo);
        if (excelDocuments.getTotalElements() == 0) {
            if (pageInfo.getFilter() != null)
                throw new ResponseException("excel document have no content , change filter before trying again");
            else
                throw new ResponseException("excel document have no content please import excel before trying again");
        }


        ExcelError excelError = this.validateExcelDocument(pageInfo.getTableName(), excelDocuments.getContent());
        Map<String, Object> response = new HashMap<>();
        response.put("info", excelError);
        response.put("currentPage", excelDocuments.getNumber());
        response.put("totalItems", excelDocuments.getTotalElements());
        response.put("totalPages", excelDocuments.getTotalPages());

        return response;
    }

    @Override
    public ExcelError validateAllExcelDocumentByExcelName(String excelName) {
        String excelCollectionId = excelCollectionGlobalDtoService.findByNameElseThrowException(excelName).getId();
        List<ExcelDocument> excelDocuments = excelDocumentGlobalDtoService.findAllByExcelCollection_Id(excelCollectionId);
        if (excelDocuments.size() == 0) {
            throw new ResponseException("excel document have no content please import excel before trying again");
        }
        return this.validateExcelDocument(excelName, excelDocuments);
    }

    @Override
    public Map<String, Object> validateExcelDocumentById(String id) {
        ExcelDocument excelDocument = excelDocumentGlobalDtoService.findByIdElseThrowException(id);
        ExcelError excelError = this.validateExcelDocument(excelDocument.getExcelCollection().getName(), List.of(excelDocument));
        Map<String, Object> response = new HashMap<>();
        response.put("rows", excelError.getBody());
        return response;
    }

    @Override
    public Map<String, Object> updateExcelDocumentWithValidation(ExcelDocumentGlobalDto excelDocumentGlobalDto) {

        excelDocumentGlobalDtoService.updateValidation(excelDocumentGlobalDto);

        ExcelDocument currentEntity = excelDocumentGlobalDtoService.findByIdElseThrowException(excelDocumentGlobalDto.getId());
        currentEntity.getContents().forEach((k, v) -> {
            if (excelDocumentGlobalDto.getContents().containsKey(k))
                currentEntity.getContents().put(k, excelDocumentGlobalDto.getContents().get(k).trim());
        });
        excelDocumentGlobalDtoService.save(currentEntity);

        return this.validateExcelDocumentById(currentEntity.getId());
    }

    private ExcelError validateExcelDocument(String excelName, List<ExcelDocument> excelDocuments) {
        ExcelTableGlobalDto excelTableGlobalDto = excelTableGlobalDtoService.findAllByNameElseThrowException(excelName);

        Map<String, List<String>> foreignKeys = excelDataService.getAllForeignKeyValues(excelName);
        Map<String, List<String>> uniqueKeysTable = excelDataService.getAllUniqueKeyValues(excelName);
        Map<String, List<String>> uniqueKeysExcelFile = excelDataService.getAllExcelFileUniqueKeyValues(excelName);
        Map<String, ColumnValidationDto> columnValidations = this.getColumnValidationsFromExcelTableGlobalDto(excelTableGlobalDto);
        List<String> requiredColumns = this.getRequiredColumnFromExcelTableGlobalDto(excelTableGlobalDto);

        List<ExcelDocumentError> excelDocumentErrors  = excelDocuments.parallelStream()
                .map(this::convertExcelDocumentToExcelDocumentError)
                .collect(Collectors.toList());

        if(isTableValidationExists(excelTableGlobalDto.getTableValidations(),"REQUIRED")){
            excelDocumentErrors = excelDocumentErrors.parallelStream()
                    .map(document -> this.checkRequiredExcelDocument(document, requiredColumns))
                    .collect(Collectors.toList());
        }
        if(isTableValidationExists(excelTableGlobalDto.getTableValidations(),"FOREIGN_KEY")){
            excelDocumentErrors = excelDocumentErrors.parallelStream()
                    .map(excelDocumentError -> this.checkForeignKeyValidation(excelDocumentError, foreignKeys))
                    .collect(Collectors.toList());
        }

        if(isTableValidationExists(excelTableGlobalDto.getTableValidations(),"UNIQUE_KEY")){
            excelDocumentErrors = excelDocumentErrors.parallelStream()
                    .map(excelDocumentError -> this.checkUniqueKeysTableValidation(excelDocumentError, uniqueKeysTable))
                    .map(excelDocumentError -> this.checkUniqueKeysExcelFileValidation(excelDocumentError, uniqueKeysExcelFile))
                    .collect(Collectors.toList());
        }

        if(isTableValidationExists(excelTableGlobalDto.getTableValidations(),"TYPE")){
            excelDocumentErrors = excelDocumentErrors.parallelStream()
                    .map(excelDocumentError -> this.checkColumnValidation(excelDocumentError, columnValidations))
                    .collect(Collectors.toList());
        }

        return ExcelError.builder()
                .header(excelDocuments.get(0).getContents().keySet())
                .body(excelDocumentErrors)
                .foreignKeys(foreignKeys)
                .uniqueKeysTable(uniqueKeysTable)
                .uniqueKeysExcelFile(uniqueKeysExcelFile)
                .externalServerInsert(excelTableGlobalDto.isExternalServerInsert())
                .build();
    }

    private ExcelDocumentError convertExcelDocumentToExcelDocumentError(ExcelDocument excelDocument){
        return new ExcelDocumentError(excelDocument.getId(), excelDocument.getContents());
    }

    private ExcelDocumentError checkRequiredExcelDocument(ExcelDocumentError excelDocumentError, List<String> requiredColumns) {
        Set<String> requiredColumnsErrors = new HashSet<>();
        requiredColumns.forEach((value) -> {
            if ((excelDocumentError.getContents().get(value).isEmpty()) || excelDocumentError.getContents().get(value).equals(""))
                requiredColumnsErrors.add(value);
        });
        if (!requiredColumnsErrors.isEmpty()) {
            excelDocumentError.setRequiredColumns(requiredColumnsErrors);
            excelDocumentError.setHaveValidationError(true);
        }
        return excelDocumentError;
    }

    private ExcelDocumentError checkForeignKeyValidation(ExcelDocumentError excelDocumentError, Map<String, List<String>> foreignKeys) {
        Set<String> foreignKeysErrors = new HashSet<>();
        foreignKeys.forEach((key, value) -> {
            if (!value.contains(excelDocumentError.getContents().get(key)) && (requiredContentValidation(excelDocumentError, key)))
                foreignKeysErrors.add(key);
        });
        if (!foreignKeysErrors.isEmpty()) {
            excelDocumentError.setForeignKeys(foreignKeysErrors);
            excelDocumentError.setHaveValidationError(true);
        }

        return excelDocumentError;
    }

    private ExcelDocumentError checkUniqueKeysTableValidation(ExcelDocumentError excelDocumentError, Map<String, List<String>> uniqueKeys) {
        Set<String> uniqueKeysErrors = new HashSet<>();
        uniqueKeys.forEach((key, value) -> {
            if (!Objects.equals(excelDocumentError.getContents().get(key), "") && value.contains(excelDocumentError.getContents().get(key)) && (requiredContentValidation(excelDocumentError, key)))
                uniqueKeysErrors.add(key);
        });
        if (!uniqueKeysErrors.isEmpty()) {
            excelDocumentError.setUniqueKeys(uniqueKeysErrors);
            excelDocumentError.setHaveValidationError(true);
        }

        return excelDocumentError;
    }

    private ExcelDocumentError checkUniqueKeysExcelFileValidation(ExcelDocumentError excelDocumentError, Map<String, List<String>> uniqueKeys) {
        Set<String> uniqueKeysErrors = new HashSet<>();
        uniqueKeys.forEach((key, value) -> {
            if (!Objects.equals(excelDocumentError.getContents().get(key), "") && Collections.frequency(value, excelDocumentError.getContents().get(key)) > 1 && (requiredContentValidation(excelDocumentError, key)))
                uniqueKeysErrors.add(key);
        });
        if (!uniqueKeysErrors.isEmpty()) {
            if (!((excelDocumentError.getUniqueKeys() == null) || excelDocumentError.getUniqueKeys().isEmpty())) {
                uniqueKeysErrors.addAll(excelDocumentError.getUniqueKeys());
            }
            excelDocumentError.setUniqueKeys(uniqueKeysErrors);
            excelDocumentError.setHaveValidationError(true);
        }
        return excelDocumentError;
    }

    private ExcelDocumentError checkColumnValidation(ExcelDocumentError excelDocumentError, Map<String, ColumnValidationDto> columnValidation) {
        Set<String> columnValidations = new HashSet<>();
        columnValidation.forEach((key, value) -> {
            if (!this.columnValid(excelDocumentError.getContents().get(key), value) && (requiredContentValidation(excelDocumentError, key)))
                columnValidations.add(key);
        });
        if (!columnValidations.isEmpty()) {
            if (!((excelDocumentError.getColumnValidation() == null) || excelDocumentError.getColumnValidation().isEmpty()))
                columnValidations.addAll(excelDocumentError.getColumnValidation());
            excelDocumentError.setColumnValidation(columnValidations);
            excelDocumentError.setHaveValidationError(true);
        }
        return excelDocumentError;
    }

    private boolean requiredContentValidation(ExcelDocumentError excelDocumentError, String key) {
        if (excelDocumentError.getRequiredColumns() == null || !excelDocumentError.getRequiredColumns().contains(key))
            return true;
        return excelDocumentError.getContents().get(key) != null && !Objects.equals(excelDocumentError.getContents().get(key), "");
    }

    private List<String> getRequiredColumnFromExcelTableGlobalDto(ExcelTableGlobalDto excelTableGlobalDto) {
        List<String> result = new ArrayList<>();
        excelTableGlobalDto.getExcelColumns().forEach((value) -> {
            if (value.getRequired())
                result.add(value.getName());
        });
        return result;
    }

    private Map<String, ColumnValidationDto> getColumnValidationsFromExcelTableGlobalDto(ExcelTableGlobalDto excelTableGlobalDto) {

        Map<String, ColumnValidationDto> result = new HashMap<>();
        excelTableGlobalDto.getExcelColumns().forEach((value) -> {
            if (value.getColumnForeignKey() == null)
                result.put(value.getName(), value.getColumnValidations().parallelStream().findFirst().get());
        });
        return result;
    }

    private boolean columnValid(String value, ColumnValidationDto validation) {
        switch (validation.getType()) {
            case REGEX_PATTERN:
                if (!isRegexPattern(value, validation.getPattern()))
                    return false;
                break;
            case REGEX_DATE:
                if (!isDatePattern(value, validation.getPattern()))
                    return false;
                break;
            case NONE:
                return true;
        }
        return true;
    }

    private boolean isTableValidationExists(List<TableValidationDto> tableValidation , String validation){
        if (tableValidation.isEmpty())
            return false;
        for (TableValidationDto tValidation : tableValidation)
        {
            if (Objects.equals(tValidation.getName(), validation))
                return true;
        }
        return false;
    }
}
