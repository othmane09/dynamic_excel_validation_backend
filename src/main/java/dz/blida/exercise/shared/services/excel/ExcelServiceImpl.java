package dz.blida.exercise.shared.services.excel;

import dz.blida.exercise.relationnel.models.ColumnForeignKey;
import dz.blida.exercise.relationnel.models.ColumnUniqueKey;
import dz.blida.exercise.relationnel.models.ExcelColumn;
import dz.blida.exercise.relationnel.models.ExcelTable;
import dz.blida.exercise.relationnel.services.columnForeignKey.dto.global.ColumnForeignKeyGlobalDtoService;
import dz.blida.exercise.relationnel.services.columnUniqueKey.dto.global.ColumnUniqueKeyGlobalDtoService;
import dz.blida.exercise.relationnel.services.columnValidation.dto.global.ColumnValidationGlobalDtoService;
import dz.blida.exercise.relationnel.services.excelColumn.dto.global.ExcelColumnGlobalDtoService;
import dz.blida.exercise.relationnel.services.excelTable.dto.global.ExcelTableGlobalDtoService;
import dz.blida.exercise.relationnel.services.tableValidation.dto.models.global.TableValidationGlobalDtoService;
import dz.blida.exercise.shared.exeptions.ResponseException;
import dz.blida.exercise.shared.models.excelInfo.ExcelInfo;
import dz.blida.exercise.shared.models.excelInfo.ExcelInfoColumn;
import dz.blida.exercise.shared.models.excelInfo.ExcelInfoColumnForeignKey;
import dz.blida.exercise.shared.models.excelInfo.ExcelInfoColumnUniqueKey;
import dz.blida.exercise.shared.services.excelWizard.excelStructure.ExcelStructureService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Service
@AllArgsConstructor
public class ExcelServiceImpl implements ExcelService {


    private final DatabaseMetadataService databaseMetadataService;


    ExcelTableGlobalDtoService excelTableService;

    TableValidationGlobalDtoService tableValidationService;

    ColumnValidationGlobalDtoService columnValidationService;

    ColumnUniqueKeyGlobalDtoService columnUniqueKeyService;

    ColumnForeignKeyGlobalDtoService columnForeignKeyService;

    ExcelColumnGlobalDtoService excelColumnService;

    ExcelStructureService excelStructureService;

    @Override
    public void saveExcelTable(ExcelInfo excelInfo) {
        ExcelTable excelTable;
        if (excelInfo.getId() != null) {
            excelTable = excelTableService.findByIdElseThrowException(excelInfo.getId());
            if (checkIfBigChangeHappen(excelInfo, excelTable))
                excelStructureService.deleteExcelFile(excelTable.getName());
            deleteExcelTableRelations(excelTable);
        } else {
            excelTable = new ExcelTable();
        }
        excelTable.setName(excelInfo.getName());
        excelTable.setUrl(excelInfo.getUrl());
        excelTable.setExternalServerInsert(excelInfo.isExternalServerInsert());
        excelTable.setDescription(excelInfo.getDescription());
        excelTable.setTableValidations(tableValidationService.findByIdIn(excelInfo.getExcelValidation()));
        excelTableService.save(excelTable);

        excelInfo.getExcelInfoColumns().forEach((column) -> {
            ExcelColumn excelColumn = saveExcelColumn(column, excelTable);

            if (column.getExcelInfoColumnForeignKey() != null)
                excelColumn.setColumnForeignKey(saveForeignKey(column.getExcelInfoColumnForeignKey(), excelColumn));

            if (column.getExcelInfoColumnUniqueKey() != null)
                excelColumn.setColumnUniqueKey(saveUniqueKey(column.getExcelInfoColumnUniqueKey(), excelColumn));

            if (column.getExcelInfoColumnForeignKey() != null || column.getExcelInfoColumnUniqueKey() != null)
                excelColumnService.save(excelColumn);
        });
    }


    private ExcelColumn saveExcelColumn(ExcelInfoColumn excelInfoColumn, ExcelTable excelTable) {

        ExcelColumn excelColumn = new ExcelColumn().builder()
                .name(excelInfoColumn.getName())
                .required(excelInfoColumn.getRequired())
                .position(excelInfoColumn.getPosition())
                .columnValidations(columnValidationService.findByNameIn(List.of(excelInfoColumn.getExcelColumnValidation())))
                .excelTable(excelTable)
                .build();

        return excelColumnService.save(excelColumn);
    }

    private ColumnForeignKey saveForeignKey(ExcelInfoColumnForeignKey excelInfoColumnForeignKey, ExcelColumn excelColumn) {
        ColumnForeignKey columnForeignKey = new ColumnForeignKey().builder()
                .excelColumn(excelColumn)
                .schema(excelInfoColumnForeignKey.getSchema())
                .tableName(excelInfoColumnForeignKey.getTableName())
                .tableReferenceColumn(excelInfoColumnForeignKey.getTableReferenceColumn())
                .tableIdColumn(excelInfoColumnForeignKey.getTableIdColumn())
                .build();
        return columnForeignKeyService.save(columnForeignKey);
    }


    private ColumnUniqueKey saveUniqueKey(ExcelInfoColumnUniqueKey excelInfoColumnUniqueKey, ExcelColumn excelColumn) {
        ColumnUniqueKey columnUniqueKey = new ColumnUniqueKey().builder()
                .excelColumn(excelColumn)
                .schema(excelInfoColumnUniqueKey.getSchema())
                .tableName(excelInfoColumnUniqueKey.getTableName())
                .tableReferenceColumn(excelInfoColumnUniqueKey.getTableReferenceColumn())
                .build();
        return columnUniqueKeyService.save(columnUniqueKey);
    }

    private boolean checkIfBigChangeHappen(ExcelInfo excelInfo, ExcelTable excelTable) {

        if (!excelInfo.getName().equals(excelTable.getName()))
            return true;

        if (excelInfo.getExcelInfoColumns().size() != excelTable.getExcelColumns().size())
            return true;

        for (ExcelColumn excelColumn : excelTable.getExcelColumns()) {
            if (excelInfo.getExcelInfoColumns().stream().noneMatch(excelInfoColumn -> Objects.equals(excelInfoColumn.getName(), excelColumn.getName())))
                return true;
        }

        return false;
    }


    @Override
    public void excelInfoValidation(ExcelInfo excelInfo) {

        List<Integer> columnPositions = new ArrayList<>();

        if (excelInfo.getId() != null) {
            ExcelTable excelTableTemp = excelTableService.findByIdElseThrowException(excelInfo.getId());
            if (!Objects.equals(excelTableTemp.getName(), excelInfo.getName()) && excelTableService.existsByName(excelInfo.getName()))
                throw new ResponseException("table name all ready exist");
        } else if (excelTableService.existsByName(excelInfo.getName()))
            throw new ResponseException("table name all ready exist");

        if (excelInfo.getName().length() < 2)
            throw new ResponseException("table name is smaller then 2 chars");

        if (tableValidationService.findByIdIn(excelInfo.getExcelValidation()).size() != excelInfo.getExcelValidation().size()) {
            throw new ResponseException("table validation does not match with our database requirement");
        }
        if (excelInfo.getExcelInfoColumns().isEmpty())
            throw new ResponseException("table column should be at least 1");

        excelInfo.getExcelInfoColumns().forEach((column) -> {
            columnPositions.add(column.getPosition());
            columnValidation(column);
        });
        positionValidation(columnPositions);
    }

    private void columnValidation(ExcelInfoColumn column) {
        if (column.getName().length() < 2)
            throw new ResponseException("column name is smaller then 2 chars");

        if (column.getExcelColumnValidation() == null || Objects.equals(column.getExcelColumnValidation(), "") || !columnValidationService.existsByName(column.getExcelColumnValidation()))
            throw new ResponseException("column validation doesn't exit");
        foreignKeyValidation(column.getExcelInfoColumnForeignKey());
        uniqueKeyValidation(column.getExcelInfoColumnUniqueKey());
    }

    private void positionValidation(List<Integer> columnPositions) {
        Integer maxPosition = columnPositions.size();
        columnPositions.forEach((position) -> {
            if (((Collections.frequency(columnPositions, position)) > 1) || position > maxPosition) {
                throw new ResponseException("there is at least 2 column with the same position");
            }
        });
    }

    private void foreignKeyValidation(ExcelInfoColumnForeignKey excelInfoColumnForeignKey) {
        if (excelInfoColumnForeignKey != null) {
            String schema = excelInfoColumnForeignKey.getSchema();
            String table = excelInfoColumnForeignKey.getTableName();
            String field = excelInfoColumnForeignKey.getTableReferenceColumn();
            databaseValidation(schema, table, field);
            if (!databaseMetadataService.getAllColumnsBySchemaAndTable(schema, table).contains(excelInfoColumnForeignKey.getTableIdColumn()))
                throw new ResponseException("table id " + excelInfoColumnForeignKey.getTableIdColumn() + " note exist");
        }
    }

    private void uniqueKeyValidation(ExcelInfoColumnUniqueKey excelInfoColumnUniqueKey) {
        if (excelInfoColumnUniqueKey != null) {
            String schema = excelInfoColumnUniqueKey.getSchema();
            String table = excelInfoColumnUniqueKey.getTableName();
            String field = excelInfoColumnUniqueKey.getTableReferenceColumn();
            databaseValidation(schema, table, field);
        }
    }

    private void databaseValidation(String schema, String table, String field) {
        if (!databaseMetadataService.getAllSchemas().contains(schema))
            throw new ResponseException("schema name " + schema + " note exist");
        if (!databaseMetadataService.getAllTableNamesBySchema(schema).contains(table))
            throw new ResponseException("table name " + table + " note exist");
        if (!databaseMetadataService.getAllColumnsBySchemaAndTable(schema, table).contains(field))
            throw new ResponseException("column name " + field + " note exist");
    }

    @Override
    public void deleteExcelTable(String tableId) {
        ExcelTable excelTable = excelTableService.findByIdElseThrowException(tableId);
        excelStructureService.deleteExcelFile(excelTable.getName());
        deleteExcelTableRelations(excelTable);
        excelTableService.delete(excelTable);
    }

    private void deleteExcelTableRelations(ExcelTable excelTable) {
        excelTable.getExcelColumns().forEach((column) -> {
            column.getColumnValidations().removeAll(column.getColumnValidations());
            excelColumnService.save(column);
        });
        excelColumnService.deleteAll(excelTable.getExcelColumns());
        excelTable.getTableValidations().removeAll(excelTable.getTableValidations());
        excelTable.getExcelColumns().removeAll(excelTable.getExcelColumns());
        excelTableService.save(excelTable);

    }
}