package dz.blida.exercise.shared.services.excelWizard.importFile;


import dz.blida.exercise.noSql.models.ExcelCollection;
import dz.blida.exercise.noSql.models.ExcelDocument;
import dz.blida.exercise.noSql.models.ExcelField;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface ImportFileService {

    void importExcelFile(MultipartFile files, String name) throws IOException;

    XSSFSheet convertExcelFileToWorkSheet(MultipartFile files) throws IOException;

    Map<String, String> convertExcelFileRowToMapOfString(XSSFRow excelRow, List<ExcelField> excelHeader);

    List<ExcelField> convertExcelFileRowsToListOfExcelFields(XSSFRow excelRow, ExcelCollection excelCollection);

    List<ExcelDocument> convertExcelFileRowsToListOfExcelDocuments(XSSFSheet excelContent, List<ExcelField> excelHeaders, ExcelCollection excelCollection);

    String getCellValueAsString(Cell cell);

    void excelValidation(String name);

}
