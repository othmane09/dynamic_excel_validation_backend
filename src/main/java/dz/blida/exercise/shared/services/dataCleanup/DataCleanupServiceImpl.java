package dz.blida.exercise.shared.services.dataCleanup;

import dz.blida.exercise.noSql.models.ExcelCollection;
import dz.blida.exercise.noSql.models.ExcelDocument;
import dz.blida.exercise.noSql.models.ExcelField;
import dz.blida.exercise.noSql.services.excelCollection.dto.global.ExcelCollectionGlobalDtoServiceImpl;
import dz.blida.exercise.noSql.services.excelDocument.dto.global.ExcelDocumentGlobalDtoServiceImpl;
import dz.blida.exercise.noSql.services.excelField.dto.global.ExcelFieldGlobalDtoServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class DataCleanupServiceImpl implements DataCleanupService {

    ExcelFieldGlobalDtoServiceImpl excelFieldService;

    ExcelDocumentGlobalDtoServiceImpl excelDocumentService;

    ExcelCollectionGlobalDtoServiceImpl excelCollectionService;


    @Override
    public void clearAllDataWithNoReference() {
        List<ExcelCollection> excelCollections = excelCollectionService.findAll();
        deleteAllExcelDocumentWithNoReference(excelCollections);
        deleteAllExcelFieldWithNoReference(excelCollections);
    }

    @Override
    public void deleteAllExcelFieldWithNoReference(List<ExcelCollection> excelCollections) {
        Optional<List<ExcelField>> excelFields = excelFieldService.findByExcelCollectionIsNotIn(excelCollections);
        excelFields.ifPresent(fields -> excelFieldService.deleteAll(fields));
    }

    @Override
    public void deleteAllExcelDocumentWithNoReference(List<ExcelCollection> excelCollections) {
        Optional<List<ExcelDocument>> excelDocuments = excelDocumentService.findByExcelCollectionIsNotIn(excelCollections);
        excelDocuments.ifPresent(documents -> excelDocumentService.deleteAll(documents));

    }
}
