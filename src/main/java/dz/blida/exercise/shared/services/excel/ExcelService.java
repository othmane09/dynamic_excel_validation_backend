package dz.blida.exercise.shared.services.excel;

import dz.blida.exercise.shared.models.excelInfo.ExcelInfo;

public interface ExcelService {

    void excelInfoValidation(ExcelInfo excelInfo);

    void saveExcelTable(ExcelInfo excelInfo);

    void deleteExcelTable(String tableId);

}
