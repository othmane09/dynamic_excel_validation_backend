package dz.blida.exercise.shared.controllers.excelWizard;

import dz.blida.exercise.noSql.services.excelCollection.ExcelCollectionService;
import dz.blida.exercise.shared.response.Response;
import dz.blida.exercise.shared.response.ResponseService;
import dz.blida.exercise.shared.services.excelWizard.importFile.ImportFileService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/excel/import")
@AllArgsConstructor
public class ImportFileController {

    ImportFileService importExcelFile;

    ExcelCollectionService excelCollectionService;

    @PostMapping()
    public ResponseEntity<Response> importExcelFile(@RequestParam(value = "file", required = true) MultipartFile file,
                                                    @RequestParam(value = "") String excelName) throws IOException {

        importExcelFile.excelValidation(excelName);
        importExcelFile.importExcelFile(file, excelName);
        return ResponseService.success("Excel Uploaded Successfully");
    }

}
