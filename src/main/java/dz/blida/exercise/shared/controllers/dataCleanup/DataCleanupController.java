package dz.blida.exercise.shared.controllers.dataCleanup;

import dz.blida.exercise.shared.response.Response;
import dz.blida.exercise.shared.response.ResponseService;
import dz.blida.exercise.shared.services.dataCleanup.DataCleanupService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/data_cleanup")
@AllArgsConstructor
public class DataCleanupController {

    private DataCleanupService dataCleanupService;

    @GetMapping()
    public ResponseEntity<Response> dataCleanup() {
        dataCleanupService.clearAllDataWithNoReference();
        return ResponseService.success("Excel cleaned Successfully", Map.of("header", "cleaned"));
    }

}
