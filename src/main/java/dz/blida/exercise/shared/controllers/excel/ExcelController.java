package dz.blida.exercise.shared.controllers.excel;

import dz.blida.exercise.shared.models.excelInfo.ExcelInfo;
import dz.blida.exercise.shared.response.Response;
import dz.blida.exercise.shared.response.ResponseService;
import dz.blida.exercise.shared.services.excel.DatabaseMetadataService;
import dz.blida.exercise.shared.services.excel.ExcelService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/excel")
@AllArgsConstructor
public class ExcelController {

    @Autowired
    private DatabaseMetadataService databaseMetadataService;

    @Autowired
    private ExcelService excelService;

    public ExcelController() {
    }

    //create and update
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Response> save(@RequestBody ExcelInfo excelInfo) {
        excelService.excelInfoValidation(excelInfo);
        excelService.saveExcelTable(excelInfo);
        return ResponseService.success("Excel saved Successfully", Map.of("header", excelInfo));
    }

    @GetMapping("/datatable_structure")
    public Object getDatabaseStructure() {
        return databaseMetadataService.getDatabaseStructure();
    }

    @GetMapping("/tables_by_schema/{schema}")
    public List<String> getAllTableNamesBySchema(@PathVariable String schema) {
        return databaseMetadataService.getAllTableNamesBySchema(schema);
    }

    @GetMapping("/schemas")
    public List<String> getAllSchemas() {
        return databaseMetadataService.getAllSchemas();
    }

    @GetMapping("/column_by_schema_and_table/{schema}/{tableName}")
    public List<String> getAllColumnsOfTableBySchema(@PathVariable String schema, @PathVariable String tableName) {
        return databaseMetadataService.getAllColumnsBySchemaAndTable(schema, tableName);
    }

}
