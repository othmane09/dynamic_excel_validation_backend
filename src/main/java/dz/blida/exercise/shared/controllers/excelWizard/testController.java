package dz.blida.exercise.shared.controllers.excelWizard;

import dz.blida.exercise.shared.response.Response;
import dz.blida.exercise.shared.response.ResponseService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/test")
@AllArgsConstructor
public class testController {

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Response> read(@RequestBody List<Object> request) {
        return ResponseService.success(Map.of("k", "m"));
    }
}
