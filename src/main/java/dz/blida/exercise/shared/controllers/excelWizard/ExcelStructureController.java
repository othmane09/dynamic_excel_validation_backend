package dz.blida.exercise.shared.controllers.excelWizard;

import dz.blida.exercise.noSql.models.Status;
import dz.blida.exercise.noSql.services.excelField.dto.global.ExcelFieldGlobalDto;
import dz.blida.exercise.shared.response.Response;
import dz.blida.exercise.shared.response.ResponseService;
import dz.blida.exercise.shared.services.excelWizard.excelStructure.ExcelStructureService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/excel/excel_structure")
@AllArgsConstructor
public class ExcelStructureController {

    ExcelStructureService excelStructureService;

    @GetMapping()
    public ResponseEntity<Response> getHeaders(@RequestParam() String excelName) {
        return ResponseService.success("header", excelStructureService.getExcelHeaderStructure(excelName));
    }

    @GetMapping(value = "/status")
    public ResponseEntity<Response> getExcelFileStatus(@RequestParam() String excelName) {
        Map<String, Status> result = Map.of("status", excelStructureService.getExcelFileStatus(excelName));
        return ResponseService.success("Excel Uploaded Successfully", result);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public ResponseEntity<Response> deleteExcelFile(@RequestParam() String excelName) {
        excelStructureService.deleteExcelFile(excelName);
        return ResponseService.success("Excel deleted Successfully");
    }

    @RequestMapping(value = "/{excelName}", method = RequestMethod.POST)
    public ResponseEntity<Response> updateHeader(@RequestBody List<ExcelFieldGlobalDto> header,
                                                 @PathVariable("excelName") String excelName) {

        excelStructureService.structureValidation(excelName);
        excelStructureService.updateExcelStructure(excelName, header);
        return ResponseService.success("Excel Uploaded Successfully");
    }
}
