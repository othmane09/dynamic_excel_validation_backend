package dz.blida.exercise.shared.controllers.excelWizard;

import dz.blida.exercise.noSql.services.excelDocument.dto.global.ExcelDocumentGlobalDto;
import dz.blida.exercise.shared.models.PageInfo;
import dz.blida.exercise.shared.response.Response;
import dz.blida.exercise.shared.response.ResponseService;
import dz.blida.exercise.shared.services.excelWizard.excelValidation.ExcelValidationService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/excel/excel_validation")
@AllArgsConstructor
public class ExcelValidationController {
    ExcelValidationService excelValidationService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Response> read(@RequestBody PageInfo pageInfo) {
        Map<String, Object> data = excelValidationService.validateExcelDocumentByPage(pageInfo);
        return ResponseService.success(data);
    }

    @RequestMapping(value = "/update_excel_document", method = RequestMethod.PUT, consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Response> updateDto(@Valid @RequestBody ExcelDocumentGlobalDto entity) {
        return ResponseService.success("ENTITY UPDATED SUCCESSFULLY", excelValidationService.updateExcelDocumentWithValidation(entity));
    }

}
