package dz.blida.exercise.shared.controllers.excelWizard;


import dz.blida.exercise.shared.response.Response;
import dz.blida.exercise.shared.response.ResponseService;
import dz.blida.exercise.shared.services.excelWizard.excelInsertValidData.ExcelInsertValidDataService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/excel/excel_insert_valid_data")
@AllArgsConstructor
public class ExcelInsertValidDataController {

    ExcelInsertValidDataService excelInsertValidDataService;

    @GetMapping(value = "/external_api/{excelName}")
    public ResponseEntity<Response> insertValidDataToExternalClient(@PathVariable() String excelName) {
        var  response =  excelInsertValidDataService.insertData(excelName, "EXTERNAL_SERVER")
                .flatMap(result -> Mono.just(ResponseService.success("Excel inserted Successfully")))
                .onErrorResume(error -> Mono.just(ResponseService.bad_request("ERROR FROM API SERVER ??,  CONTACT YOUR CLIENT"))).block();
        if (response == null)
            return ResponseService.success("Excel inserted Successfully") ;
        return response;
    }

    @GetMapping(value = "/download/{excelName}")
    public Mono<?> insertValidDataDownloadFile(@PathVariable() String excelName) {
        return excelInsertValidDataService.insertData(excelName, "DOWNLOAD");
    }
}
