package dz.blida.exercise.shared.utility;

import lombok.experimental.UtilityClass;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import java.util.regex.Pattern;

@UtilityClass
public class StringValidation {
    public static boolean isRegexPattern(String value, String regexPattern) {
        Pattern pattern = Pattern.compile(regexPattern);
        if (value == null) {
            return false;
        }
        return pattern.matcher(value).matches();
    }

    public static boolean isDatePattern(String value, String datePattern) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(datePattern).withResolverStyle(ResolverStyle.STRICT);
        try {
            LocalDate.parse(value, dateTimeFormatter);
            return true;
        } catch (DateTimeParseException e) {
            return false;
        }
    }
}
