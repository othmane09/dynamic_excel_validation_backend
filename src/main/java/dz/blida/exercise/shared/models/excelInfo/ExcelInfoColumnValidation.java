package dz.blida.exercise.shared.models.excelInfo;

import lombok.Data;

@Data
public class ExcelInfoColumnValidation {
    private String name;
}