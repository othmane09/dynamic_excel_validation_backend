package dz.blida.exercise.shared.models.datatableMetadata;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class DatabaseMetadata {
    private List<Schema> schemas = new ArrayList<>();

    public void addSchema(Schema schema) {
        this.schemas.add(schema);
    }
}