package dz.blida.exercise.shared.models;

import lombok.Data;
import lombok.experimental.SuperBuilder;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Data
@SuperBuilder
public class ExcelError {

    private String id;

    private Set<String> header;

    private List<ExcelDocumentError> body;

    private boolean externalServerInsert;

    private Map<String, List<String>> foreignKeys;

    private Map<String, List<String>> uniqueKeysTable;

    private Map<String, List<String>> uniqueKeysExcelFile;

}
