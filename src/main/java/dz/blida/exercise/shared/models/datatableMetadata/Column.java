package dz.blida.exercise.shared.models.datatableMetadata;

import lombok.Data;

@Data
public class Column {
    private String columnName;
}
