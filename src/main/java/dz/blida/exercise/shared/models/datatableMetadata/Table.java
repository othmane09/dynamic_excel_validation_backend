package dz.blida.exercise.shared.models.datatableMetadata;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Table {
    private String tableName;
    private List<Column> columns = new ArrayList<>();

    public void addTable(Column column) {
        this.columns.add(column);
    }
}