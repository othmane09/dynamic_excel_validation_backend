package dz.blida.exercise.shared.models.excelInfo;

import lombok.Data;

@Data
public class ExcelInfoColumnUniqueKey {
    private String schema;
    private String tableName;
    private String tableReferenceColumn;
}