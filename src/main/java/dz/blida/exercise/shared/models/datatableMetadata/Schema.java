package dz.blida.exercise.shared.models.datatableMetadata;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Schema {
    private String schemaName;
    private List<Table> tables = new ArrayList<>();

    public void addTable(Table table) {
        this.tables.add(table);
    }
}