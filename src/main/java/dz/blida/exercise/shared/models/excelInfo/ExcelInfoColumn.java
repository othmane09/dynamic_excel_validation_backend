package dz.blida.exercise.shared.models.excelInfo;

import lombok.Data;

@Data
public class ExcelInfoColumn {
    private String name;
    private Integer position;
    private Boolean required;
    private ExcelInfoColumnForeignKey excelInfoColumnForeignKey;
    private ExcelInfoColumnUniqueKey excelInfoColumnUniqueKey;
    private String excelColumnValidation;

}