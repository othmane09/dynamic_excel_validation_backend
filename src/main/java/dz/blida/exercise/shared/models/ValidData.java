package dz.blida.exercise.shared.models;

import lombok.Data;

import java.util.Map;

@Data
public class ValidData {
    String id;
    Map<String, String> contents;

    public ValidData(String id, Map<String, String> contents) {
        this.id = id;
        this.contents = contents;
    }

}