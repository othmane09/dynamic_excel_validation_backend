package dz.blida.exercise.shared.models.excelInfo;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ExcelInfo {
    private String id;
    private String name;
    private boolean externalServerInsert;
    private String description;
    private String url;
    private List<String> excelValidation = new ArrayList<>();
    private List<ExcelInfoColumn> excelInfoColumns = new ArrayList<>();
}
