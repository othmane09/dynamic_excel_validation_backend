package dz.blida.exercise.shared.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExcelDocumentError {

    private String id;
    private Map<String, String> contents;

    private Set<String> requiredColumns;

    private Set<String> foreignKeys;

    private Set<String> uniqueKeys;

    private Set<String> columnValidation;

    private boolean haveValidationError;

    public ExcelDocumentError(String id, Map<String, String> contents) {
        this.id = id;
        this.contents = contents;
        this.haveValidationError = false;
    }


}
