package dz.blida.exercise.shared.models;

import lombok.Data;

import java.util.Map;

@Data
public class PageInfo {

    private int page;
    private int size;
    private String globalFilter;
    private String tableName;
    private Map<String, String> filter;
}


