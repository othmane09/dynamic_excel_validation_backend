package dz.blida.exercise.shared.exeptions.handlers;


import dz.blida.exercise.shared.exeptions.DeleteException;
import dz.blida.exercise.shared.exeptions.NoRecordException;
import dz.blida.exercise.shared.exeptions.NullParameterException;
import dz.blida.exercise.shared.exeptions.ResponseException;
import dz.blida.exercise.shared.response.Response;
import org.springframework.http.ResponseEntity;

public interface CustomExceptionHandler {


    ResponseEntity<Response> handleResponseException(ResponseException e);

    ResponseEntity<Response> handleNoRecordException(NoRecordException e);

    ResponseEntity<Response> handleNullPointerException(NullParameterException e);

    ResponseEntity<Response> handleDeleteExceptionException(DeleteException e);

}
