package dz.blida.exercise.shared.exeptions.handlers;


import dz.blida.exercise.shared.exeptions.DeleteException;
import dz.blida.exercise.shared.exeptions.NoRecordException;
import dz.blida.exercise.shared.exeptions.NullParameterException;
import dz.blida.exercise.shared.exeptions.ResponseException;
import dz.blida.exercise.shared.response.Response;
import dz.blida.exercise.shared.response.ResponseService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@AllArgsConstructor
@ControllerAdvice
public class CustomExceptionHandlerImpl implements CustomExceptionHandler {

    @ExceptionHandler(value = ResponseException.class)
    public ResponseEntity<Response> handleResponseException(ResponseException e) {
        return ResponseService.bad_request(e.getMessage());
    }

    @ExceptionHandler(value = NoRecordException.class)
    public ResponseEntity<Response> handleNoRecordException(NoRecordException e) {
        return ResponseService.bad_request("No records found in : " + e.getMessage() + " Please check back later.");
    }

    @ExceptionHandler(value = NullParameterException.class)
    public ResponseEntity<Response> handleNullPointerException(NullParameterException e) {
        return ResponseService.bad_request("Parameter  : " + e.getMessage() + " have no value");
    }

    @ExceptionHandler(value = DeleteException.class)
    public ResponseEntity<Response> handleDeleteExceptionException(DeleteException e) {
        return ResponseService.bad_request("Cant Delete   : " + e.getMessage() + " Contact Admin For More Information");
    }
}
