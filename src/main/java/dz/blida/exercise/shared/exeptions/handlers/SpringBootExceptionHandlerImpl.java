package dz.blida.exercise.shared.exeptions.handlers;

import dz.blida.exercise.shared.response.Response;
import dz.blida.exercise.shared.response.ResponseService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MultipartException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@ControllerAdvice
@AllArgsConstructor
public class SpringBootExceptionHandlerImpl implements SpringBootExceptionHandler {


    @ExceptionHandler(MultipartException.class)
    public ResponseEntity<Response> handleMultipartException(MultipartException ex) {
        return ResponseService.bad_request("Please select a file");
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Response> handleValidationExceptions(MethodArgumentNotValidException ex) {
        return ResponseService.bad_request("INVALID DATA", getValidationExceptionsErrors(ex));
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Response> handleConstraintViolation(ConstraintViolationException ex) {
        try {
            return ResponseService.bad_request("contain violation", ex.getConstraintViolations().stream().map(ConstraintViolation::getMessage).collect(Collectors.toList()));
        } catch (Exception e) {
            return ResponseService.bad_request(ex.getMessage());
        }
    }

    private Map<String, List<String>> getValidationExceptionsErrors(BindException ex) {
        Map<String, List<String>> entityError = new HashMap<>();
        ex.getBindingResult().getFieldErrors().forEach(error -> {
            List<String> fieldErrors = new ArrayList<>();
            if (entityError.containsKey(error.getField()))
                fieldErrors = entityError.get(error.getField());
            fieldErrors.add(error.getDefaultMessage());
            entityError.put(error.getField(), fieldErrors);
        });
        return entityError;
    }
}
