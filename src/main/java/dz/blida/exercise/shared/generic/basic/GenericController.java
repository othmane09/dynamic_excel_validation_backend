package dz.blida.exercise.shared.generic.basic;

import com.fasterxml.jackson.databind.ObjectMapper;
import dz.blida.exercise.shared.generic.dto.basic.GenericBasicDtoService;
import dz.blida.exercise.shared.response.Response;
import dz.blida.exercise.shared.response.ResponseService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static dz.blida.exercise.shared.utility.GlobalFunctions.getNullPropertyNames;


public class GenericController<ENTITY, SERVICE extends GenericBasicDtoService, ID> {

    SERVICE entityService;
    @Autowired
    private ObjectMapper objectMapper;

    public GenericController(SERVICE entityService) {
        this.entityService = entityService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Response> read(@PathVariable("id") ID id) {
        return ResponseService.success(entityService.findByIdElseThrowException(id));
    }

    @GetMapping()
    public ResponseEntity<Response> readAll() {
        return ResponseService.success(entityService.findAllElseThrowException());
    }

    @RequestMapping(method = RequestMethod.POST, consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Response> store(@Valid @RequestBody ENTITY entity) {
        return ResponseService.success("ENTITY ADDED SUCCESSFULLY", entityService.save(entity));
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT, consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Response> update(@PathVariable("id") ID id, @Valid @RequestBody ENTITY entity) {
        ENTITY currentEntity = (ENTITY) entityService.findByIdElseThrowException(id);
        BeanUtils.copyProperties(entity, currentEntity, getNullPropertyNames(entity));
        return ResponseService.success("ENTITY UPDATED SUCCESSFULLY", entityService.save(currentEntity));
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Response> delete(@PathVariable("id") ID id) {
        entityService.delete(id);
        return ResponseService.success("Deleted successfully!");
    }

    public Map<String, Object> convertObjectToMap(Object object) {
        return objectMapper.convertValue(object, Map.class);
    }

    public List<Map<String, Object>> convertListOfObjectsToListOfMaps(List<Object> objects) {
        List<Map<String, Object>> maps = new ArrayList<>();
        for (Object object : objects) {
            Map<String, Object> map = convertObjectToMap(object);
            maps.add(map);
        }
        return maps;
    }

    public SERVICE getEntityService() {
        return entityService;
    }
}