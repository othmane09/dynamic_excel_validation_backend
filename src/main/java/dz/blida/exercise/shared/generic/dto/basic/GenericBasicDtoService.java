package dz.blida.exercise.shared.generic.dto.basic;

import dz.blida.exercise.shared.generic.basic.GenericService;

import java.util.List;

public interface GenericBasicDtoService<ENTITY, BASIC_DTO, ID> extends GenericService<ENTITY, ID> {

    BASIC_DTO findByIdAsBasicDto(ID id);

    BASIC_DTO findByIdAsBasicDtoElseThrowException(ID id);

    List<BASIC_DTO> findAllAsBasicDtos();

    List<BASIC_DTO> findAllAsBasicDtosElseThrowException();

    BASIC_DTO saveFromBasicDto(BASIC_DTO entity);

    List<BASIC_DTO> saveAllFromBasicDtos(Iterable<BASIC_DTO> entities);

    void deleteFromBasicDto(BASIC_DTO entity);

    void deleteAllFromBasicDto(Iterable<BASIC_DTO> entities);

}
