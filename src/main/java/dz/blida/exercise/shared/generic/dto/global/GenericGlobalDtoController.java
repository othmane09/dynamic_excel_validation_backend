package dz.blida.exercise.shared.generic.dto.global;

import dz.blida.exercise.shared.generic.dto.basic.GenericBasicDtoController;
import dz.blida.exercise.shared.response.Response;
import dz.blida.exercise.shared.response.ResponseService;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static dz.blida.exercise.shared.utility.GlobalFunctions.getNullPropertyNames;

public class GenericGlobalDtoController<ENTITY, GLOBAL_DTO, BASIC_DTO, SERVICE extends GenericGlobalDtoService, ID> extends GenericBasicDtoController<ENTITY, BASIC_DTO, SERVICE, ID> {

    public GenericGlobalDtoController(SERVICE entityService) {
        super(entityService);
    }

    @GetMapping("/global_dto/{id}")
    public ResponseEntity<Response> readGlobalDto(@PathVariable("id") ID id) {
        return ResponseService.success(getEntityService().findByIdAsGlobalDtoElseThrowException(id));
    }

    @GetMapping("/global_dto")
    public ResponseEntity<Response> readAllGlobalDtos() {
        return ResponseService.success(getEntityService().findAllAsGlobalDtosElseThrowException());
    }

    @RequestMapping(value = "/global_dto", method = RequestMethod.POST, consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Response> storeGlobalDto(@Valid @RequestBody GLOBAL_DTO entity) {
        return ResponseService.success("ENTITY ADDED SUCCESSFULLY", getEntityService().saveFromGlobalDto(entity));
    }


    @RequestMapping(value = "/global_dto/{id}", method = RequestMethod.PUT, consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Response> updateGlobalDto(@PathVariable("id") ID id, @Valid @RequestBody GLOBAL_DTO entity) {
        GLOBAL_DTO currentEntity = (GLOBAL_DTO) getEntityService().findByIdAsGlobalDtoElseThrowException(id);
        BeanUtils.copyProperties(entity, currentEntity, getNullPropertyNames(entity));
        return ResponseService.success("ENTITY UPDATED SUCCESSFULLY", getEntityService().saveFromGlobalDto(currentEntity));
    }
}