package dz.blida.exercise.shared.generic.dto.basic;

import dz.blida.exercise.shared.generic.basic.GenericController;
import dz.blida.exercise.shared.response.Response;
import dz.blida.exercise.shared.response.ResponseService;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static dz.blida.exercise.shared.utility.GlobalFunctions.getNullPropertyNames;

public class GenericBasicDtoController<ENTITY, BASIC_DTO, SERVICE extends GenericBasicDtoService, ID> extends GenericController<ENTITY, SERVICE, ID> {

    public GenericBasicDtoController(SERVICE entityService) {
        super(entityService);
    }

    @GetMapping("/basic_dto/{id}")
    public ResponseEntity<Response> readBasicDto(@PathVariable("id") ID id) {
        return ResponseService.success(getEntityService().findByIdAsBasicDtoElseThrowException(id));
    }

    @GetMapping("/basic_dto")
    public ResponseEntity<Response> readAllBasicDtos() {
        return ResponseService.success(getEntityService().findAllAsBasicDtosElseThrowException());
    }

    @RequestMapping(value = "/basic_dto", method = RequestMethod.POST, consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Response> storeBasicDto(@Valid @RequestBody BASIC_DTO entity) {
        return ResponseService.success("ENTITY ADDED SUCCESSFULLY", getEntityService().saveFromBasicDto(entity));
    }


    @RequestMapping(value = "/basic_dto/{id}", method = RequestMethod.PUT, consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Response> updateBasicDto(@PathVariable("id") ID id, @Valid @RequestBody BASIC_DTO entity) {
        BASIC_DTO currentEntity = (BASIC_DTO) getEntityService().findByIdAsBasicDtoElseThrowException(id);
        BeanUtils.copyProperties(entity, currentEntity, getNullPropertyNames(entity));
        return ResponseService.success("ENTITY UPDATED SUCCESSFULLY", getEntityService().saveFromBasicDto(currentEntity));
    }
}