package dz.blida.exercise.shared.generic.dto.global;

import dz.blida.exercise.shared.generic.dto.basic.GenericBasicDtoMapper;

import java.util.List;

public interface GenericGlobalDtoMapper<ENTITY, BASIC_DTO, GLOBAL_DTO> extends GenericBasicDtoMapper<ENTITY, BASIC_DTO> {

    GLOBAL_DTO entityToGlobalDto(ENTITY entity);

    List<GLOBAL_DTO> entitiesToGlobalDtos(List<ENTITY> entities);

    ENTITY globalDtoToEntity(GLOBAL_DTO entityDto);

    List<ENTITY> globalDtosToEntities(List<GLOBAL_DTO> entitiesDto);

}
