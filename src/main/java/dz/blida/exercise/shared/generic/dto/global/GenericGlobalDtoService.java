package dz.blida.exercise.shared.generic.dto.global;

import dz.blida.exercise.shared.generic.dto.basic.GenericBasicDtoService;

import java.util.List;

public interface GenericGlobalDtoService<ENTITY, BASIC_DTO, GLOBAL_DTO, ID> extends GenericBasicDtoService<ENTITY, BASIC_DTO, ID> {

    GLOBAL_DTO findByIdAsGlobalDto(ID id);

    GLOBAL_DTO findByIdAsGlobalDtoElseThrowException(ID id);

    List<GLOBAL_DTO> findAllAsGlobalDtos();

    List<GLOBAL_DTO> findAllAsGlobalDtosElseThrowException();

    GLOBAL_DTO saveFromGlobalDto(GLOBAL_DTO entity);

    List<GLOBAL_DTO> saveAllFromGlobalDtos(Iterable<GLOBAL_DTO> entities);

    void deleteFromGlobalDto(GLOBAL_DTO entity);

    void deleteAllFromGlobalDto(Iterable<GLOBAL_DTO> entities);

}
