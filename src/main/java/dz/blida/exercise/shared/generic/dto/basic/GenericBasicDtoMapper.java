package dz.blida.exercise.shared.generic.dto.basic;

import java.util.List;

public interface GenericBasicDtoMapper<ENTITY, BASIC_DTO> {

    BASIC_DTO entityToBasicDto(ENTITY entity);

    List<BASIC_DTO> entitiesToBasicDtos(List<ENTITY> entities);

    ENTITY basicDtoToEntity(BASIC_DTO entityDto);

    List<ENTITY> basicDtosToEntities(List<BASIC_DTO> entitiesDto);

}
