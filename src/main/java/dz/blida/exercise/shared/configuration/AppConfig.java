package dz.blida.exercise.shared.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

import javax.sql.DataSource;


@Configuration
public class AppConfig {

    @Value("${postgres.port}")
    private int postgresPort;

    @Value("${postgres.host}")
    private String postgresHost;

    @Value("${postgres.username}")
    private String postgresUsername;

    @Value("${postgres.password}")
    private String postgresPassword;

    @Value("${postgres.db_name}")
    private String postgresDbName;

    @Bean
    public DataSource dataSource() {
        return DataSourceBuilder.create()
                .url("jdbc:postgresql://" + postgresHost + ":" + postgresPort + "/" + postgresDbName)
                .username(postgresUsername)
                .password(postgresPassword)
                .driverClassName("org.postgresql.Driver")
                .build();
    }

    @Bean
    public WebClient webClient() {
        return WebClient.create();
    }

}