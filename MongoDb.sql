MongoDb

--------------------select--------------------

db.student.find({ "name" : "jhon"})

---------------------and--------------------------

db.student.find({ $and : [ { "name" : "othmane" }, { "mail" : "a@b.c" } ] })


---------------------or--------------------------

db.student.find({ $or : [ { "name" : "othmane" }, { "mail" : "a@b.c" } ] })


---------------------in--------------------------

db.student.find({ "name" : { $in : [ "othmane", "jhon" ] } })

---------------------update ------------------------
-- update 1 document (first)
db.student.update(
    {
        "name": "othmane"
    },
    {
        $set: {
            "mail": "othmane2@gmail.com"
        }
      
    }
)
------------------- update many -----------------------
-- update all document where name  =  othmane
db.student.updateMany(
    {
        "name": "othmane"
    },
    {
        $set: {
            "mail": "othmane2@gmail.com"
        }
      
    }
)

------------------- delete  -----------------------
db.student.remove(
    {
        "name": "othmane"
    },
)
