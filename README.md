### dynamic excel validation
****** 
- This is an application that gives you the ability validate your Excel file automatically just by given the structure of the file like columns anf foreign keys
- when the Excel is valid you have 2 choice
- 1 create your own backend web service and send this valid rows to him
- 2 download the valid rows


### Installation dependencies
****** 
- JAVA 11
- maven
- mongoDb


### Some dependencies chosen
****** 
- postgres :  database source
- mongo :  database source
- Swagger :  Documentation
- Apache Poi : excel exportation
- Lombok : Cleaner and easier for developer
- mapstruct : For mapping


### Installation Note
****** 
- 1 configure your postgres and mongo db in  : application-dev
  
### Frontend Application

go visit : https://gitlab.com/othmane09/dynamic_excel_validation_frontend