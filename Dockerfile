
FROM openjdk:11

COPY target/exercise-0.0.1.jar app.jar

EXPOSE 8088

ENTRYPOINT ["java", "-jar", "/app.jar"]